<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('seoexpert.logistic');

$IBLOCK_ID = intval($arParams['IBLOCK_ID']);
if ($IBLOCK_ID <= 0) return false;

$pageSize = intval($arParams['PAGE_SIZE']);
if ($pageSize <= 0) $pageSize = 15;

$filter = array(
    'CREATED_DATE_FROM' => '',
    'CREATED_DATE_TO' => '',
    'SHIPMENT_DATE' => '',
    'DELIVERY_DATE' => '',
	'ORDER_NUMBER' => '',
    'RECIPIENT' => '',
);

/* parse input */
foreach (array('CREATED_DATE_FROM', 'CREATED_DATE_TO', 'SHIPMENT_DATE', 'DELIVERY_DATE') as $param) {
    if (isset($_REQUEST[$param]) && DateTime::createFromFormat('d.m.Y', $_REQUEST[$param]) !== false) {
        $filter[$param] = trim($_REQUEST[$param]);
    }
}
if (isset($_REQUEST['ORDER_NUMBER']) && trim($_REQUEST['ORDER_NUMBER']) != '') {
    $filter['ORDER_NUMBER'] = trim($_REQUEST['ORDER_NUMBER']);
}
if (isset($_REQUEST['RECIPIENT']) && trim($_REQUEST['RECIPIENT']) != '') {
    $filter['RECIPIENT'] = trim($_REQUEST['RECIPIENT']);
}

$items = array();

$arFilter = array(
	'IBLOCK_ID' => $IBLOCK_ID
);

foreach ($filter as $param => $value) {
    if ($value != '') {
        switch ($param) {
            case 'CREATED_DATE_FROM':
                $arFilter['>=PROPERTY_DATA_SOZDANIYA'] = $DB->FormatDate($value . ' 00:00:00', 'DD.MM.YYYY HH:MI:SS', 'YYYY-MM-DD HH:MI:SS');
                break;
            case 'CREATED_DATE_TO':
                $arFilter['<=PROPERTY_DATA_SOZDANIYA'] = $DB->FormatDate($value . ' 23:59:59', 'DD.MM.YYYY HH:MI:SS', 'YYYY-MM-DD HH:MI:SS');
                break;
            case 'SHIPMENT_DATE':
                $arFilter['>=PROPERTY_SHIPMENT_DATE'] = $DB->FormatDate($value . ' 00:00:00', 'DD.MM.YYYY HH:MI:SS', 'YYYY-MM-DD HH:MI:SS');
                $arFilter['<=PROPERTY_SHIPMENT_DATE'] = $DB->FormatDate($value . ' 23:59:59', 'DD.MM.YYYY HH:MI:SS', 'YYYY-MM-DD HH:MI:SS');
                break;
            case 'DELIVERY_DATE':
                $arFilter['>=PROPERTY_DELIVERY_DATE'] = $DB->FormatDate($value . ' 00:00:00', 'DD.MM.YYYY HH:MI:SS', 'YYYY-MM-DD HH:MI:SS');
                $arFilter['<=PROPERTY_DELIVERY_DATE'] = $DB->FormatDate($value . ' 23:59:59', 'DD.MM.YYYY HH:MI:SS', 'YYYY-MM-DD HH:MI:SS');
                break;
            case 'ORDER_NUMBER':
                $arFilter['PROPERTY_ORDER_NUMBER'] = '%' . $value . '%';
                break;
            case 'RECIPIENT':
                $arFilter['PROPERTY_RECIPIENT'] = array();
                $dbItems = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => CONTRAGENT_IBLOCK_ID, 'PROPERTY_NAME' => '%' . $value . '%'), false, false, array('ID'));
                while ($arItem = $dbItems->GetNext()) {
                    if (!in_array($arItem['ID'], $arFilter['PROPERTY_RECIPIENT'])) {
                        $arFilter['PROPERTY_RECIPIENT'][] = $arItem['ID'];
                    }
                }
                break;
        }
    }
}

$arNavParams = array(
    'nPageSize' => $pageSize,
    'bShowAll' => 'Y',
);
$dbItems = CIBlockElement::GetList(array('ID' => 'DESC'), $arFilter, false, $arNavParams);
while ($obItem = $dbItems->GetNextElement()) {
	$item = array();
    $fields = $obItem->GetFields();
    $item['ID'] = $fields['ID'];
	$props = $obItem->GetProperties();
    foreach ($props as $param => $v) {
        $value = $v['VALUE'];
        // link to iblock element
        if ($v['PROPERTY_TYPE'] == 'E') {
            $dbLinkedItem = CIBlockElement::GetById($value);
            if ($linkedItem = $dbLinkedItem->Fetch()) {
                $value = $linkedItem['NAME'];
            } else {
                $value = '';
            }
        }
        // link to user
        if ($v['PROPERTY_TYPE'] == 'S' && $v['USER_TYPE'] == 'UserID') {
            $dbUser = CUser::GetById($value);
            if ($user = $dbUser->Fetch()) {
                $value = $user['NAME'];
            } else {
                $value = '';
            }
        }
        $item[$param] = $value;
    }
    //tmp 
    $item['NUMBER'] = $fields['NAME'];
    $item['PERMISSIONS'] = \Logistic\Application::getPermissions($item['ID']);
	$items[] = $item;
}

$arResult = array(
	'ITEMS' => $items,
	'FILTER' => $filter
);

$arResult['NAV_STRING'] = $dbItems->GetPageNavStringEx($navComponentObject, '', '', 'N');
$arResult['NAV_CACHED_DATA'] = null;
$arResult['NAV_RESULT'] = $dbItems;

$this->IncludeComponentTemplate();
