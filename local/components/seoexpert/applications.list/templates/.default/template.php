<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
?>

<script src="/bitrix/templates/aspro_mshop/js_new/floatThead/dist/jquery.floatThead.min.js"></script>
<link rel="stylesheet" href="/bitrix/templates/aspro_mshop/js_new/jquery-ui.css">
<link rel="stylesheet" href="/bitrix/templates/aspro_mshop/js_new/jquery-ui-timepicker-addon.css">
<script src="/bitrix/templates/aspro_mshop/js_new/jquery-ui.min.js"></script>
<script src="/bitrix/templates/aspro_mshop/js_new/jquery.ui.datepicker-ru.js"></script>

<form method="get" id="filter-form">
    <div class="clearfix searchfilt">
        <h3>Показать за период</h3>
        <div class="left">
            <div class="form-control">
                <label><span>С&nbsp;</span></label>
                <input type="text" id="CREATED_DATE_FROM" value="<?=$arResult['FILTER']['CREATED_DATE_FROM'];?>" name='CREATED_DATE_FROM'>
            </div>
            <div class="form-control">
                <label><span>По&nbsp;</span></label>
                <input type="text" id="CREATED_DATE_TO" value="<?=$arResult['FILTER']['CREATED_DATE_TO'];?>" name='CREATED_DATE_TO'>
            </div>
            <div class="form-control" style="width: 100%;">
            	<a class="button button-cancel" href="javascript:void(0);" style="display: none;" onclick="if (confirm('Выбранные заявки будут отменены.')) return false;">Отменить отмеченные</a>&nbsp;
            </div>
        </div>
        <div class="right">
            <div class="form-control">
                <label><span>Номер заказа&nbsp;</span></label>
                <input type="text" name="ORDER_NUMBER" value="<?=$arResult['FILTER']['ORDER_NUMBER'];?>">
            </div>
            <div class="form-control">
                <label><span>Дата отправления&nbsp;</span></label>
                <input type="text" name="SHIPMENT_DATE" value="<?=$arResult['FILTER']['SHIPMENT_DATE'];?>" id="SHIPMENT_DATE">
            </div>
            <div class="form-control">
                <label><span>Дата получения&nbsp;</span></label>
                <input type="text" name="DELIVERY_DATE" value="<?=$arResult['FILTER']['DELIVERY_DATE'];?>" id="DELIVERY_DATE">
            </div>
            <div class="form-control">
                <label><span>Получатель&nbsp;</span></label>
                <input type="text" name="RECIPIENT" value="<?=$arResult['FILTER']['RECIPIENT'];?>">
            </div>
            <div class="form-control" style="width: 100%; text-align: right;">
                <input type="button" class="button" value="Найти" onclick="$('#filter-form').submit();">&nbsp;
                <input type="button" class="button" value="Сбросить" onclick="window.location.href = '<?=strtok($_SERVER['REQUEST_URI'], '?');?>';">
            </div>
        </div>
    </div>
</form>

<br />
<div class="personal_wrapper applications-table table-responsive">
    <table class="table">
        <thead>
            <tr>                 
                <th>Действия</th>
                <th>Номер заявки</th>
                <th>Дата создания</th>
                <th>Статус</th>
                <th>Заказчик</th>
                <th>Кол-во мест</th>
                <th>Вес нетто, кг</th>
                <th>Вес брутто, кг</th>
                <th>Объем, куб. м</th>
                <th>Пользователь</th>
                <th>Отправитель</th>
                <th>Адрес отправителя</th>
                <th>Контакт отправителя</th>
                <th>Дата отправления</th>
                <th>Получатель</th>
                <th>Адрес получателя</th>
                <th>Контакт получателя</th>
                <th>Дата доставки</th>
            </tr>
        </thead>
        <tbody>
            <?foreach ($arResult['ITEMS'] as $item):?>
                <tr>
                    <td style="text-align: center; padding: 0px 0px 0px 20px !important;">
                    	<div style="width: 88px; height: 30px;">
                        <?if ($item['PERMISSIONS']['EDIT'] == 'Y'):?>
                            <a class="action-button button-edit" href="/personal/applications/edit/<?=$item['ID'];?>/" title="Редактировать заявку"></a>&nbsp;
                        <?else:?>
                            <a class="action-button button-view" href="/personal/applications/view/<?=$item['ID'];?>/" title="Просмотр заявки"></a>&nbsp;
                        <?endif;?>
                        <a class="action-button button-repeat" href="/personal/applications/repeat/<?=$item['ID'];?>/" title="Повторить заявку"></a>&nbsp;
                        <a class="action-button button-pdf" href="/personal/applications/pdf/<?=$item['ID'];?>/" target="_blank" title="PDF"></a></div>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$item['NUMBER'];?></td>
                    <td><?=$item['DATE'];?></td>
                    <td><?=$item['STATUS'];?></td>
                    <td><?=$item['CUSTOMER'];?></td>
                    <td><?=$item['PALLET_LOAD_Q'];?></td>
                    <td><?=$item['NET_WEIGHT'];?></td>
                    <td><?=$item['GROSS_WEIGHT'];?></td>
                    <td><?=$item['VOLUME'];?></td>
                    <td><?=$item['CREATED_BY'];?></td>
                    <td><?=$item['SENDER'];?></td>
                    <td><?=$item['SENDER_ADDRESS'];?></td>
                    <td><?=$item['SENDER_CONTACT'];?></td>
                    <td><?=$item['SHIPMENT_DATE'];?></td>
                    <td><?=$item['RECIPIENT'];?></td>
                    <td><?=$item['RECIPIENT_ADDRESS'];?></td>
                    <td><?=$item['RECIPIENT_CONTACT'];?></td>
                    <td><?=$item['DELIVERY_DATE'];?></td>
                </tr>
            <?endforeach;?>
        </tbody>
    </table>
</div>

<div style="clear: both;"></div>
<?=$arResult['NAV_STRING'];?>

<script type="text/javascript">
    $(document).ready(function() {
        var $tableBlock = $('.table-responsive'),
            $table = $tableBlock.find(".table");

        $table.each(function(idx) {
            if ($("thead", this).length) {
                if ($("tbody tr", this).length > 4) {
                    $(this).addClass("table-header-sticky");
                }
            }
        });

        $tableBlock.find(".table-header-sticky").floatThead({
            scrollContainer: function($table) {
                return $table.closest(".table-responsive");
            }
        });

        $(".applications-table tbody tr").each(function() {
            var $this = $(this);
            if ($this.find("input[type=radio]").prop("checked")) {
                $this.closest("tr").addClass("selected")

            }
        });
        $(".applications-table tbody tr").on("click", function() {
            var $this = $(this),
                $table = $this.closest("table"),
                $tr = $table.find("tr");
            $tr.removeClass("selected");
            $this.addClass("selected").find("input[type=radio]").prop('checked', true);

            if ($this.addClass("selected").find("input[type=radio]").attr('status') != 30) {
                $('[name=DELETE]').hide();
                $('[name=REPEAT]').show();
            } else {
                $('[name=DELETE]').show();
                $('[name=REPEAT]').show();
            }
        });
        $.extend($.datepicker, {
            travelRanges: function(options) {
                var settings = {
                    target: '.range-dates',
                    maxDateToBook: '360',
                    //dafaultDate: new Date(),
                    populateFirst: true
                };

                $.extend(settings, options);

                $(settings.target).datepicker({
                    //minDate: '0',
                    onSelect: function(selectedDate) {
                        var self = this,
                            one = $(settings.target).eq(0),
                            two = $(settings.target).eq(1);
                        if ($(self).is(one)) {
                            var newMaxDate = $(settings.target).datepicker('getDate');
                            newMaxDate.setDate($(this).datepicker('getDate').getDate() + settings.maxDateToBook);
                            two.datepicker("change", {
                                "minDate": $(settings.target).datepicker('getDate'),
                                "maxDate": newMaxDate
                            });
                        }
                    }
                });
                if (settings.populateFirst) {
                    $(settings.target + ":first").datepicker('setDate', settings.dafaultDate);
                }
            }
        });

        $.datepicker.setDefaults($.datepicker.regional['ru']);

        var $from = $("#CREATED_DATE_FROM"),
            $to = $("#CREATED_DATE_TO"),
            $dateOpt = $("#SHIPMENT_DATE"),
            $datePol = $("#DELIVERY_DATE");

        $from.add($to).addClass('range1');
        $dateOpt.add($datePol).addClass('range2');

        $.datepicker.travelRanges({
            target: ".range1",
            populateFirst: false
        });
        $.datepicker.travelRanges({
            target: ".range2",
            populateFirst: false
        });

        var pickerid = "";
        $(document).on("click", function(e) {
            var cl = $(e.target);

            if (!cl.data("datepicker") && $(".ui-datepicker").is(":visible") && cl.parents().filter('.ui-corner-all').length === 0) {
                $("#" + pickerid).datepicker("hide")
            }
            if (cl.hasClass("range1") || cl.hasClass("range2")) {
                pickerid = cl.attr("id")
            } else {
                if (cl.attr("id") == pickerid) {
                    $("#ui-datepicker-div").addClass("123123").show();
                }
            }
        });
    });
</script>
