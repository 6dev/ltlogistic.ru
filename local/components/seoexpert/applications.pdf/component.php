<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$ELEMENT_ID = intval($_REQUEST['ELEMENT_ID']);
if ($ELEMENT_ID <= 0) die('');

\Bitrix\Main\Loader::includeModule('seoexpert.logistic');
$perm = \Logistic\Application::getPermissions($ELEMENT_ID);
if ($perm['VIEW'] != 'Y') return false;

$arResult = \Logistic\Application::fetchById($ELEMENT_ID);
$arResult['LISTS'] = \Logistic\Application::fetchLists($ELEMENT_ID);

include($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/tcpdf/tcpdf.php');
global $APPLICATION;
$APPLICATION->RestartBuffer();

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$fontname = TCPDF_FONTS::addTTFfont($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/tcpdf/fonts/Arial-Unicode-Regular.ttf', 'TrueTypeUnicode', '');
$pdf->SetFont($fontname, '', 10, '', true);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('');
$pdf->SetTitle($arResult['NUMBER'] . ' от ' . $arResult['DATE']);
$pdf->SetSubject('');
$pdf->SetKeywords('');
$pdf->SetHeaderData('e14a9102e496f9e5b11e3527f4f09409.jpg', PDF_HEADER_LOGO_WIDTH);
$pdf->SetHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->SetFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->AddPage();

ob_start();
$this->IncludeComponentTemplate();
$html = ob_get_contents();
ob_end_clean();

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output($arResult['NUMBER'] . '.pdf', 'I');
