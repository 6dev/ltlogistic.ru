<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
?>

<h2 style="text-align: center; margin-top: 0; padding-top: 0;">Паллетный лист к заявке <?=$arResult['NUMBER'];?> от <?=$arResult['DATE'];?></h1>
<p></p>
<table cellpadding="3">
   <tbody>
      <tr>
         <td border="1">Дата передачи груза в ТК</td>
         <td border="1"><?=$arResult['SHIPMENT_DATE'];?></td>
      </tr>
      <tr>
         <td border="1">Поставщик (продавец):</td>
         <td border="1"><?=$arResult['CUSTOMER'];?>, ИНН <?=$arResult['CUSTOMER.INN'];?></td>
      </tr>
      <tr>
         <td border="1">Грузоотправитель</td>
         <td border="1"><?=$arResult['SENDER'];?>, ИНН <?=$arResult['SENDER.INN'];?></td>
      </tr>
      <tr>
         <td border="1">Заказ покупателя</td>
         <td border="1"><?=$arResult['ORDER_NUMBER'];?></td>
      </tr>
      <tr>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td border="1">Город назначения</td>
         <td border="1"></td>
      </tr>
      <tr>
         <td border="1">Грузополучатель</td>
         <td border="1"><?=$arResult['RECIPIENT'];?>, ИНН <?=$arResult['RECIPIENT.INN'];?></td>
      </tr>
      <tr>
         <td border="1">Адрес доставки</td>
         <td border="1"><?=$arResult['RECIPIENT_ADDRESS'];?></td>
      </tr>
      <tr>
         <td border="1">Дата доставки (план):</td>
         <td border="1"><?=$arResult['DELIVERY_DATE'];?></td>
      </tr>
      <tr>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td border="1">Наименование груза</td>
         <td border="1"><?=$arResult['DESCRIPTION'];?></td>
      </tr>
      <tr>
         <td border="1">Температурный режим</td>
         <td border="1"><?=$arResult['THERMAL_RANGE'];?></td>
      </tr>
      <?
      $i1 = 0;
      $i2 = 0;
      foreach ($arResult['LISTS']['PALLET_LOAD']['ELEMENTS'] as $k) {
         if ($k['PACKAGE_TYPE'] == 34425) {
            $i1++;
            $i2 += $k['LOAD'];
         }

      }
      ?>
      <tr>
         <td border="1">Кол-во мест на паллете (в партии):</td>
         <td border="1"><?=$i1;?></td>
      </tr>
      <tr>
         <td border="1">№ паллеты (количество паллет в партии)</td>
         <td border="1"><?=$i2;?></td>
      </tr>
      <tr>
         <td border="1">Сопроводительные документы</td>
         <?
            $s = '';
            $s .= '№ ТОРГ 12: ' . $arResult['TORG_12_NUMBER'] . '<br />';
            $s .= '№ ТН: ' . $arResult['TN_NUMBER'] . '<br />';
            $s .= '№ ТТН: ' . $arResult['TTN_NUMBER'] . '<br />';
            $s = rtrim($s, '<br />');
         ?>
         <td border="1"><?=$s;?></td>
      </tr>
      <tr>
         <td></td>
         <td></td>
      </tr>
   </tbody>
</table>
</tbody>
</table>
