<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
?>

<h2 style="text-align: center; margin-top: 0; padding-top: 0;">Заявка на осуществление перевозки <?=$arResult['NUMBER'];?> от <?=$arResult['DATE'];?></h2>
<p></p>
<table cellpadding="3">
   <tbody>
      <tr>
         <td border="1">Заказчик</td>
         <td border="1"><?=$arResult['CUSTOMER'];?>, ИНН <?=$arResult['CUSTOMER.INN'];?></td>
      </tr>
      <tr>
         <td border="1">Исполнитель</td>
         <td border="1"><?=COption::GetOptionString('seoexpert.logistic', 'pdf_contractor');?></td>
      </tr>
      <tr>
         <td border="1">Тип транспорта</td>
         <td border="1">Автотранспорт</td>
      </tr>
      <tr>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td border="1">Адрес места загрузки<br />(приема продукции на склад)</td>
         <td border="1"><?=$arResult['SENDER_ADDRESS'];?></td>
      </tr>
      <tr>
         <td border="1">Ответственное лицо при загрузке</td>
         <td border="1"><?=$arResult['SENDER_CONTACT'];?></td>
      </tr>
      <tr>
         <td border="1">Дата и время загрузки (приема продукции на склад)</td>
         <td border="1"><?=$arResult['SHIPMENT_DATE'];?></td>
      </tr>
      <tr>
         <td border="1">Грузоотправитель</td>
         <td border="1"><?=$arResult['SENDER'];?>, ИНН <?=$arResult['SENDER.INN'];?></td>
      </tr>
      <tr>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td border="1">Адрес места разгрузки</td>
         <td border="1"><?=$arResult['RECIPIENT_ADDRESS'];?></td>
      </tr>
      <tr>
         <td border="1">Ответственное лицо при разгрузке</td>
         <td border="1"><?=$arResult['RECIPIENT_CONTACT'];?></td>
      </tr>
      <tr>
         <td border="1">Дата и время разгрузки</td>
         <td border="1"><?=$arResult['DELIVERY_DATE'];?></td>
      </tr>
      <tr>
         <td border="1">Грузополучатель</td>
         <td border="1"><?=$arResult['RECIPIENT'];?>, ИНН <?=$arResult['RECIPIENT.INN'];?></td>
      </tr>
      <tr>
         <td></td>
         <td></td>
      </tr>
      <tr>
         <td border="1">Количество погрузочных мест</td>
         <td border="1"><?=$arResult['PALLET_LOAD_Q'];?></td>
      </tr>
      <tr>
         <td border="1">Вес брутто и объем</td>
         <td border="1"><?=floatval($arResult['GROSS_WEIGHT']);?> кг, <?=floatval($arResult['VOLUME']);?> куб. м</td>
      </tr>
      <tr>
         <td border="1">Оценочная стоимость</td>
         <td border="1"><?=floatval($arResult['ESTIMATED_VALUE']);?> руб.</td>
      </tr>
      <tr>
         <td border="1">Комментарий к грузу</td>
         <td border="1"><?=$arResult['COMMENT'];?></td>
      </tr>
   </tbody>
</table>
<h4>Описание груза</h4>
<table cellpadding="3">
   <tbody>
      <tr>
         <td border="1">Наименование</td>
         <td border="1"><?=$arResult['DESCRIPTION'];?></td>
      </tr>
      <tr>
         <td border="1">Особые условия</td>
         <?
            $s = '';
            if ($arResult['KEEP_DRY'] == 'Y') $s .= 'Беречь от влаги<br />';
            if ($arResult['KEEP_AWAY_FROM_RADIATION'] == 'Y') $s .= 'Беречь от излучения<br />';
            if ($arResult['KEEP_AWAY_FROM_HEAT'] == 'Y') $s .= 'Беречь от нагрева<br />';
            if ($arResult['FLAMMABLE'] == 'Y') $s .= 'Огнеопасный<br />';
            if ($arResult['FRAGILE'] == 'Y') $s .= 'Хрупкий<br />';
            if ($arResult['INCONSEQUENTIAL_LOSS'] != '') $s .= 'Допустимые потери: ' . $arResult['INCONSEQUENTIAL_LOSS'] . '<br />';
            if ($arResult['THERMAL_RANGE'] != '') $s .= 'Температурный режим: ' . $arResult['THERMAL_RANGE'] . '<br />';
            $s = rtrim($s, '<br />');
         ?>
         <td border="1"><?=$s;?></td>
      </tr>
      <tr>
         <td border="1">Сопроводительные документы</td>
         <?
            $s = '';
            $s .= '№ ТОРГ 12: ' . $arResult['TORG_12_NUMBER'] . '<br />';
            $s .= '№ ТН: ' . $arResult['TN_NUMBER'] . '<br />';
            $s .= '№ ТТН: ' . $arResult['TTN_NUMBER'] . '<br />';
            $s = rtrim($s, '<br />');
         ?>
         <td border="1"><?=$s;?></td>
      </tr>
   </tbody>
</table>
<h4>Оплата</h4>
<table cellpadding="3">
   <tbody>
      <!--<tr>
         <td border="1" width="30%">Условия и сроки оплаты</td>
         <td border="1" colspan="3" width="70%"></td>
      </tr>-->
      <tr>
         <td border="1" width="60%">Наименование</td>
         <td border="1" width="20%">Сумма услуг</td>
         <td border="1" width="20%">Валюта</td>
      </tr>
      <?foreach ($arResult['LISTS']['SERVICES']['ELEMENTS'] as $service):?>
      <tr>
         <?
            foreach ($arResult['LISTS']['SERVICES']['PROPERTIES'] as $prop) {
               if ($prop['CODE'] != 'SERVICE_TYPE') continue;
               foreach ($prop['VALUES'] as $id => $value) {
                  if ($id == $service['SERVICE_TYPE']) {
                     $service['SERVICE_TYPE'] = $value;
                     break;
                  }
               }
            }
            foreach ($arResult['LISTS']['SERVICES']['PROPERTIES'] as $prop) {
               if ($prop['CODE'] != 'CURRENCY') continue;
               foreach ($prop['VALUES'] as $id => $value) {
                  if ($id == $service['CURRENCY']) {
                     $service['CURRENCY'] = $value;
                     break;
                  }
               }
            }
         ?>
         <td border="1"><?=$service['SERVICE_TYPE'];?></td>
         <td border="1" align="right"><?=$service['AMOUNT'];?></td>
         <td border="1"><?=$service['CURRENCY'];?></td>
      </tr>
      <?endforeach;?>
      
   </tbody>
</table>
<br><br>
<table cellpadding="3">
   <tbody>
      <tr>
         <td width="45%">От имени Заказчика:</td>
         <td width="10%">&nbsp;</td>
         <td width="45%" align="right">От имени Исполнителя:</td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
      <tr>
         <td>
            <table>
               <tr>
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td align="center" style="border-top:1px solid #000"><font size="6">должность</font></td>
                  <td width="5">&nbsp;</td>
                  <td align="center" style="border-top:1px solid #000"><font size="6">подпись</font></td>
                  <td width="5">&nbsp;</td>
                  <td align="center" style="border-top:1px solid #000"><font size="6">расшифровка подписи</font></td>
               </tr>
            </table>
         </td>
         <td>&nbsp;</td>
         <td>
            <table>
               <tr>
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td align="center" style="border-top:1px solid #000"><font size="6">должность</font></td>
                  <td width="5">&nbsp;</td>
                  <td align="center" style="border-top:1px solid #000"><font size="6">подпись</font></td>
                  <td width="5">&nbsp;</td>
                  <td align="center" style="border-top:1px solid #000"><font size="6">расшифровка подписи</font></td>
               </tr>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<p>Заявка на осуществление перевозки <?=$arResult['NUMBER'];?> от <?=$arResult['DATE'];?></p>

