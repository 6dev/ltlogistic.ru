<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<link rel="stylesheet" href="/bitrix/templates/aspro_mshop/js_new/jquery-ui.css">
<link rel="stylesheet" href="/bitrix/templates/aspro_mshop/js_new/jquery-ui-timepicker-addon.css">
<!--<link rel="stylesheet" href="/bitrix/templates/aspro_mshop/css/bootstrap-chosen.css">-->
<script src="/bitrix/templates/aspro_mshop/js_new/jquery-ui.min.js"></script>
<script src="/bitrix/templates/aspro_mshop/js_new/jquery.ui.datepicker-ru.js"></script>
<script src="/bitrix/templates/aspro_mshop/js_new/jquery-ui-timepicker-addon.js"></script>
<script src="/bitrix/templates/aspro_mshop/js_new/jquery-ui-timepicker-addon.js"></script>
<!--<script src="//harvesthq.github.io/chosen/chosen.jquery.js"></script>-->
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="/bitrix/templates/aspro_mshop/js_new/ru.js"></script>
<style>
	.form-body .form-head {
    margin-top: 8px;
    margin-left: 15px;
    border-top: 1px solid #ccc;
    padding-top: 13px;
}
.right_block .form-body .PALLET_LOAD-wrapper .form-control,
.right_block .form-body .FREIGHT_TRAIN-wrapper .form-control,
.right_block .form-body .SERVICES-wrapper .form-control {
	width: 10.5%;
	padding: 0 0 0 15px;
}
.right_block .form-body .SERVICES-wrapper .form-control_SERVICE_TYPE {
	width: 50% !important;
}
</style>


<form name="application-form" id="application-form" method="post" enctype="multipart/form-data" action="<?=$_SERVER['REQUEST_URI'];?>">
	<div class="form-head">
		<?if ($arParams['MODE'] == 'EDIT' || $arParams['MODE'] == 'VIEW'):?>
			<h4><?if ($arParams['MODE'] == 'EDIT') echo 'Изменение'; else echo 'Просмотр';?> заявки 
				<span style="color: red;"><?=$arResult['NUMBER'];?></span> от <?=$arResult['DATE'];?><br />
				Статус: <?=$arResult['STATUS'];?>
			</h4>
		<?else:?>
			<h4>Новая заявка б/н от <?=date('d.m.Y');?></h4>
		<?endif;?>
	</div>
	<div class="form-result"></div>
	<?if ($arParams['ELEMENT_ID'] > 0 && $arResult['PERMISSIONS']['VIEW'] == 'Y' && $arParams['MODE'] != 'REPEAT'):?>
		<div class="actions" style="margin-bottom: 15px; margin-top: -7px;">
			<?if ($arResult['PERMISSIONS']['CANCEL'] == 'Y'):?>
			<a class="button cancel-button" href="javascript:void(0);" target="_blank" onclick="cancelApplication(<?=$arParams['ELEMENT_ID'];?>)">Отменить</a>&nbsp;
			<?endif;?>
			<a class="button" href="/personal/applications/pdf/<?=$arParams['ELEMENT_ID'];?>/" target="_blank">PDF</a>&nbsp;
			<a class="button" href="/personal/applications/pallet-list/<?=$arParams['ELEMENT_ID'];?>/" target="_blank">Паллетный лист</a>
		</div>
	<?endif;?>
	<div class="form-body clearfix">
		<div class="form-control">
			<label><span>Заказчик&nbsp;<span class="star">*</span></span></label>
			<select name="CUSTOMER" class="select2-ajax" data-url="/ajax/data.php?q=1">
				<?if ($arResult['ID'] > 0 && $arResult['CUSTOMER.ID'] > 0):?>
					<option value="<?=$arResult['CUSTOMER.ID'];?>"><?=$arResult['CUSTOMER'];?></option>
				<?endif;?>
			</select>
			
		</div>
		<div class="form-control">
			<label><span>Пользователь&nbsp;<span class="star">*</span></span></label>
			<input type="text" class="inputtext" disabled value="<?=$GLOBALS['CONTACT']['NAME'];?>">
		</div>
		<div title="Отправителя можно выбрать если выбран заказчик." class="form-control">
			<label><span>Отправитель&nbsp;<span class="star">*</span></span></label>
			<select name="SENDER" class="select2-ajax">
				<?if ($arResult['ID'] > 0 && $arResult['SENDER.ID'] > 0):?>
					<option value="<?=$arResult['SENDER.ID'];?>"><?=$arResult['SENDER'];?></option>
				<?endif;?>
			</select>
		</div>
		<div title="Адрес отправителя можно выбрать если выбран отправитель." class="form-control">
			<label><span>Адрес отправителя&nbsp;<span class="star">*</span></span></label>
			<select name="SENDER_ADDRESS" class="select2-ajax">
				<?if ($arResult['ID'] > 0 && $arResult['SENDER_ADDRESS.ID'] > 0):?>
					<option value="<?=$arResult['SENDER_ADDRESS.ID'];?>"><?=$arResult['SENDER_ADDRESS'];?></option>
				<?endif;?>
			</select>
		</div>
		<div title="Контактное лицо отправителя можно выбрать если выбраны заказчик и отправитель."
		class="form-control">
			<label><span>Контактное лицо отправителя</span></label>
			<select name="SENDER_CONTACT" class="select2-ajax">
				<?if ($arResult['ID'] > 0 && $arResult['SENDER_CONTACT.ID'] > 0):?>
					<option value="<?=$arResult['SENDER_CONTACT.ID'];?>"><?=$arResult['SENDER_CONTACT'];?></option>
				<?endif;?>
			</select>
		</div>
		<div class="form-control">
			<label>Отправление груза&nbsp;<span class="star">*</span></label>
			<div class="dates">
				<input type="text" name="SHIPMENT_DATE" id="date_from" value="<?=$arResult['SHIPMENT_DATE'];?>"> &nbsp;&ndash;&nbsp;
				<input type="text" name="SHIPMENT_DATE_TO" id="date_to" value="<?=$arResult['SHIPMENT_DATE_TO'];?>">
			</div>
		</div>
		<div title="Получателя можно выбрать если выбран заказчик." class="form-control">
			<label><span>Получатель&nbsp;<span class="star">*</span></span></label>
			<select name="RECIPIENT" class="select2-ajax">
				<?if ($arResult['ID'] > 0 && $arResult['RECIPIENT.ID'] > 0):?>
					<option value="<?=$arResult['RECIPIENT.ID'];?>"><?=$arResult['RECIPIENT'];?></option>
				<?endif;?>
			</select>
		</div>
		<div title="Адрес получателя можно выбрать если выбраны заказчик и получатель."
		class="form-control">
			<label><span>Адрес получателя&nbsp;<span class="star">*</span></span></label>
			<select name="RECIPIENT_ADDRESS" class="select2-ajax">
				<?if ($arResult['ID'] > 0 && $arResult['RECIPIENT_ADDRESS.ID'] > 0):?>
					<option value="<?=$arResult['RECIPIENT_ADDRESS.ID'];?>"><?=$arResult['RECIPIENT_ADDRESS'];?></option>
				<?endif;?>
			</select>
		</div>
		<div title="Контактное лицо получателя можно выбрать если выбран получатель."
		class="form-control">
			<label><span>Контактное лицо получателя</span></label>
			<select name="RECIPIENT_CONTACT" class="select2-ajax">
				<?if ($arResult['ID'] > 0 && $arResult['RECIPIENT_CONTACT.ID'] > 0):?>
					<option value="<?=$arResult['RECIPIENT_CONTACT.ID'];?>"><?=$arResult['RECIPIENT_CONTACT'];?></option>
				<?endif;?>
			</select>
		</div>
		<div class="form-control">
		<label>Получение груза&nbsp;<span class="star">*</span></label>
			<div class="dates">
				<input type="text" name="DELIVERY_DATE" id="date_from_pol" value="<?=$arResult['DELIVERY_DATE'];?>">  &nbsp;&ndash;&nbsp;
				<input type="text" name="DELIVERY_DATE_TO" id="date_to_pol" value="<?=$arResult['DELIVERY_DATE_TO'];?>">
			</div>
		</div>
		<div class="form-head"><h4>Грузовые места</h4></div>
		<?=getSectionHTML('PALLET_LOAD', $arResult['LISTS']['PALLET_LOAD']);?>

		<div class="form-head"><h4>Общие параметры груза</h4></div>
		<div class="w4 clearfix">
			<div class="form-control">
				<label><span>Описание&nbsp;<span class="star">*</span></span></label>
				<input type="text" class="inputtext" name="DESCRIPTION" value="<?=$arResult['DESCRIPTION'];?>">
			</div>
			<div class="form-control">
			<label><span>Грузовые места&nbsp;<span class="star">*</span></span></label>
			<input type="text" class="inputtext" name="PALLET_LOAD_Q" value="<?=$arResult['PALLET_LOAD_Q'];?>">
			</div>
			<div class="form-control">
				<label><span>Вес нетто, кг&nbsp;<span class="star">*</span></span></label>
				<input type="text" class="inputtext" name="NET_WEIGHT" value="<?=$arResult['NET_WEIGHT'];?>">
			</div>
			<div class="form-control">
				<label><span>Вес брутто, кг&nbsp;<span class="star">*</span></span></label>
				<input type="text" class="inputtext" name="GROSS_WEIGHT" value="<?=$arResult['GROSS_WEIGHT'];?>">
			</div>
			<div class="form-control">
				<label><span>Объем</span></label>
				<input type="text" class="inputtext" name="VOLUME" value="<?=$arResult['VOLUME'];?>">
			</div>
			<div class="form-control">
				<label><span>Оценочная стоимость&nbsp;<span class="star">*</span></span></label>
				<input type="text" class="inputtext" name="ESTIMATED_VALUE" value="<?=$arResult['ESTIMATED_VALUE'];?>">
			</div>
			<div class="form-control">
				<label><span>Грузовых негабаритных мест</span></label>
				<input type="text" class="inputtext" name="OFF_GAUGE_PALLET_LOAD" value="<?=$arResult['OFF_GAUGE_PALLET_LOAD'];?>">
			</div>
			<div class="form-control">
				<label><span>Признак негабаритности</span></label>
				<input type="text" class="inputtext" name="OFF_GAUGE_TYPE" value="<?=$arResult['OFF_GAUGE_TYPE'];?>">
			</div>
			<div class="form-control">
				<label><span>Температурный режим</span></label>
				<select class="select2-ajax" name="THERMAL_RANGE">
					<?if ($arResult['ID'] > 0 && $arResult['THERMAL_RANGE.ID'] > 0):?>
						<option value="<?=$arResult['THERMAL_RANGE.ID'];?>"><?=$arResult['THERMAL_RANGE'];?></option>
					<?endif;?>
				</select>
			</div>
			<div class="form-control">
				<label><span>Допустимые потери</span></label>
				<input name="INCONSEQUENTIAL_LOSS" type="text" class="inputtext" value="<?=$arResult['INCONSEQUENTIAL_LOSS'];?>">
			</div>
		</div>
		<div class="w5 clearfix">
			<div class="form-control">
				<label class="checkbox-label"><input name="KEEP_DRY" type="checkbox" class="inputtext" <?if ($arResult['KEEP_DRY'] == 'Y') echo 'checked="checked"';?>>
				<span>Беречь от влаги</span></label>
			</div>
			<div class="form-control">
				<label class="checkbox-label"><input name="KEEP_AWAY_FROM_RADIATION" type="checkbox" class="inputtext" <?if ($arResult['KEEP_AWAY_FROM_RADIATION'] == 'Y') echo 'checked="checked"';?>>
				<span>Беречь от излучения</span></label>
			</div>
			<div class="form-control">
				<label class="checkbox-label"><input name="KEEP_AWAY_FROM_HEAT" type="checkbox" class="inputtext" <?if ($arResult['KEEP_AWAY_FROM_HEAT'] == 'Y') echo 'checked="checked"';?>>
				<span>Беречь от нагрева</span></label>
			</div>
			<div class="form-control">
				<label class="checkbox-label"><input name="FLAMMABLE" type="checkbox" class="inputtext" <?if ($arResult['FLAMMABLE'] == 'Y') echo 'checked="checked"';?>>
				<span>Огнеопасный</span></label>
			</div>
			<div class="form-control">
				<label class="checkbox-label"><input name="FRAGILE" type="checkbox" class="inputtext" <?if ($arResult['FRAGILE'] == 'Y') echo 'checked="checked"';?>>
				<span>Хрупкий</span></label>
			</div>
		</div>
		<div class="w4 clearfix">
			<div class="form-control">
				<label><span>Номер заказа</span></label>
				<input name="ORDER_NUMBER" type="text" class="inputtext" value="<?=$arResult['ORDER_NUMBER'];?>">
			</div>
			<div class="form-control">
			<label><span>№ ТОРГ 12</span></label>
			<input name="TORG_12_NUMBER" type="text" class="inputtext" value="<?=$arResult['TORG_12_NUMBER'];?>">
			</div>
			<div class="form-control">
			<label><span>№ TH</span></label>
			<input name="TN_NUMBER" type="text" class="inputtext" value="<?=$arResult['TN_NUMBER'];?>">
			</div>
			<div class="form-control">
			<label><span>№ ТTH</span></label>
			<input name="TTN_NUMBER" type="text" class="inputtext" value="<?=$arResult['TTN_NUMBER'];?>">
			</div>
		</div>


		<div class="form-head"><h4>Товарный состав</h4></div>
		<?=getSectionHTML('FREIGHT_TRAIN', $arResult['LISTS']['FREIGHT_TRAIN']);?>

		<?
		function getSectionHTML($code, $arList) {
			$html = '<div class="' . $code . '-wrapper">';
			$inputNamePrefix = ToUpper($code);
			for ($i = 0; $i <= count($arList['ELEMENTS']); $i++) {
				$isTemplate = $i == 0;
				$hidden = $isTemplate ? ' style="display: none;" ' : '';
				$divId = $isTemplate ? ' id="' . $code . '-template" ' : '';
				$html .= '<div class="' . $code . '"' . $hidden . $divId . '>';
				$id = $isTemplate ? '-1' : $arList['ELEMENTS'][$i - 1]['ID'];
				$html .= '<input type="hidden" class="element-id-input" name="' . $inputNamePrefix . '[ID][]" value="' . $id . '" />';
				$html .= '<input type="hidden" class="element-delete-input" name="' . $inputNamePrefix . '[DELETE][]" value="N" />';

				$html .= '<div class="form-control" style="width: auto; padding-top: 23px; padding-right: 0px;"><div class="close button" title="Удалить строку" onclick="$(this).closest(\'.' . $code . '\').css(\'display\', \'none\').find(\'.element-delete-input\').val(\'Y\');">X</div></div>';

				foreach ($arList['PROPERTIES'] as $prop) {
					$value = $i == 0 ? '' : $arList['ELEMENTS'][$i - 1][$prop['CODE']];
					$html .= '<div class="form-control form-control_' . $prop['CODE'] . '">';
	                $html .= '<label><span><nobr>' . $prop['NAME'] . '</nobr></span></label>';
	                if ($prop['LIST_TYPE'] == 'L' && $prop['LINK_IBLOCK_ID'] > 0) {
	                	$html .= '<select name="' . $inputNamePrefix . '[' . $prop['CODE'] . '][]" class="inputtext">';


	                	foreach ($prop['VALUES'] as $propId => $propValue) {
	                		$selected = $propValue == $value ? ' selected="selected" ' : '';
	                		
	                		$html .= '<option value=' . $propId . '>' . $propValue . '</option>';
	                	}
	                	$html .= '</select>';
	                } else {
	                	
	                	$html .= '<input type="text" name="' . $inputNamePrefix . '[' . $prop['CODE'] . '][]" class="inputtext" value="' . $value . '">';
	                }
	                $html .= '</div>';
				}
				$html .= '</div>';
			}
			$html .= '</div>';
			$html .= '<div class="w4 clearfix"><div class="form-control"><input type="button" class="button medium" value="Добавить" title="Добавить строку" onclick="$(\'#' . $code . '-template\').clone().removeAttr(\'id\').css(\'display\', \'block\').appendTo($(\'.' . $code . '-wrapper\')).find(\'.element-id-input\').val(0);"></div></div>';
			return $html;
		}
		?>

		<div class="form-head"><h4>Услуги</h4></div>
		<?=getSectionHTML('SERVICES', $arResult['LISTS']['SERVICES']);?>

		<div class="form-head"><h4>Особые условия</h4></div>
		<div class="form-bottom">
			<div class="form-control" style="width: 100%; padding-left: 0;">
				<label><span>Комментарий&nbsp;</span></label>
				<textarea name="COMMENT"><?=$arResult['COMMENT'];?></textarea>
			</div>
			<input type="hidden" name="SUBMIT_FORM" value="Y" />
			<?if ($arParams['MODE'] == 'CREATE' || $arParams['MODE'] == 'EDIT' || $arParams['MODE'] == 'REPEAT'):?>
				<input type="button" style="margin-right: 6px;" class="button" id="SUBMIT" name="SUBMIT" value="<?if ($arParams['MODE'] == 'CREATE') echo 'Добавить'; else echo 'Сохранить';?>&nbsp;заявку" />
			<?endif;?>
			<input type="button" class="button" value="К списку заявок" onclick="location.href='/personal/applications/';" />
		</div>
	</div>
</form>
<?if ($arParams['MODE'] == 'VIEW'):?><script type="text/javascript">$(document).ready(function() { $('#application-form input, #application-form textarea, #application-form select').prop("readonly", true).prop("disabled", "disabled"); })</script><?endif;?>

<script type="text/javascript">
	$('#SUBMIT').click(function() {
		var form = $("#application-form");
		var url = form.attr("action");
		$(".form-result").removeClass().addClass("form-result");
		$.ajax({
			type: "post",
			url: url,
			data: form.serialize(),
			dataType: "json",
			success: function(data) {
				$("html, body").animate({
					scrollTop: $(".form-head").first().offset().top
				}, 300);
				$(".form-result").html(data.html);
				if (data.success == true) {
					$(".form-body, .actions").hide();
					$(".form-result").addClass("success");
				} else {
					$(".form-result").addClass("error");
				}
			}
		});
	});
</script>


<script type="text/javascript">
	function cancelApplication(id) {
		if (!confirm('Заявка будет отменена.')) return;
		var form = $("#application-form");
		var url = form.attr("action");
		$.ajax({
			type: "post",
			url: url,
			data: { ACTION: 'CANCEL', ID: id },
			dataType: "json",
			success: function(data) {
				window.location.href= '/personal/applications/view/'+id+'/';
			}
		});
	}
</script>







<script type="text/javascript">

$(function () {

var time = new Date(),
year = time.getFullYear(),
month = time.getMonth(),

<? if ($hour >= $tt) echo 'day = time.getDate()+2, // ' . $hour; ?>
<? if ($hour < $tt) echo 'day = time.getDate()+1, // ' . $hour; ?>


hour = 0, //time.getHours() + 1 > 15 ? 10 :
minutes = time.getMinutes();
//console.log(time.getHours(), day, hour  )


$.datepicker.setDefaults($.datepicker.regional['ru']);
var dateFormat = "dd.mm.yy",
timeFormat = "HH:mm",
from = $("#date_from"),
to = $("#date_to"),
fromPol = $("#date_from_pol"),
toPol = $("#date_to_pol");

$.timepicker.datetimeRange(
from,
to,
{
minDate: new Date(year, month, day, hour, 0),
hourMin: 0,
minuteMin: 0,
minuteMax: 0,
minTime: 0,
minInterval: (1000 * 60 * 60),
showMinute: false,
timeOnlyTitle: 'Выберите время',
timeText: 'Время',
hourText: 'Часы',
minuteText: 'Минуты',
secondText: 'Секунды',
currentText: 'Сейчас',
closeText: 'Закрыть',
timeFormat: timeFormat,
start: {
hourMax: 23
},
end: {
hourMax: 23
},
onSelect: function (selectedDateTime) {
var $this = $(this),
id = $this.attr("id"),
newDate = $this.datetimepicker('getDate'),
day = $this.datetimepicker('getDate').getDate(),
hour = $this.datetimepicker('getDate').getHours();
if (id == "date_from") {
//console.log(newDate, hour, $this.attr("id"));
newDate.setHours(hour + 4);
}
if (id == "date_to") {

//console.log(newDate, hour, $this.attr("id"));
newDate.setHours(hour + 2);
}
var hour1 = newDate.getHours();
//fromPol.datetimepicker('option', 'minDate', newDate);
//fromPol.datetimepicker('option', 'minDateTime', newDate);
//fromPol.datetimepicker("setDate", newDate);
}
}
);

$.timepicker.datetimeRange(
fromPol,
toPol,
{
minDate: new Date(year, month, day, hour, 0),
hourMin: 0,
minuteMin: 0,
minuteMax: 0,
minTime: 0,
minInterval: (1000 * 60 * 60),
showMinute: false,
timeOnlyTitle: 'Выберите время',
timeText: 'Время',
hourText: 'Часы',
minuteText: 'Минуты',
secondText: 'Секунды',
currentText: 'Сейчас',
closeText: 'Закрыть',
timeFormat: timeFormat,
start: {
hourMax: 23
},
end: {
hourMax: 23
}
}
);



$(document).on("click", function (e) {
var cl = $(e.target);
//                console.log("data", $(e.target).data("datepicker") )
//                console.log($(e.target).closest(".ui-datepicker").length)
if (!cl.data("datepicker") && $(".ui-datepicker").is(":visible") && !cl.parents().filter('.ui-datepicker:visible').length && cl.parents().filter('.ui-corner-all').length === 0) {
//                    console.log("visible")
$(".ui-datepicker:visible").find(".ui-datepicker-close").click();
}
});

});

$(document).ready(function(){
	$('.select2').select2();
	$(".select2-ajax").each(function() {
	    var url = '<?=$this->GetFolder();?>/ajax.php?property=' + $(this).attr('name');
	    $(this).select2({
	    	language: 'ru',
	        ajax: {
				url: url,
				dataType: 'json',
				data: function (params) {
					var query = {
						search: params.term,
						page: params.page || 1
					}
					$(".select2-ajax").each(function() {
						query[$(this).attr('name')] = $(this).val();
					});
					return query;
				}
	        },
	    });
	});
});

</script>

<style>
.ui-draggable, .ui-droppable {
	background-position: top;
}

.form-body .form-head {
	margin-top: 8px;
    margin-left: 15px;
}
.form-body .form-bottom {
	margin-left: 15px;
}
.right_block .form-body .w4 .form-control {
	width: 24%;
}
.right_block .form-body .w5 .form-control {
	width: 19%;
}

.form-result.success {
	    color: #3c763d;
    background-color: #dff0d8;

        padding: 9px 15px;
        border: 1px solid #d6e9c6;
}

.form-result.error {
    
    background-color: #f2dede;
 margin-bottom: 20px;

        padding: 9px 15px;
        border: 1px solid #ebccd1;
}
.form-result.error p {
	color: #a94442;
}
</style>