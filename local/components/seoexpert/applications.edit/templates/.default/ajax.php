<?php
error_reporting(0);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
\Bitrix\Main\Loader::includeModule('seoexpert.logistic');

$result = array(
	'results' => array(),
	'pagination' => array('more' => false)
);

$arOrder = array('NAME' => 'ASC');
$arFilter = array();
if (isset($_REQUEST['search']) && trim($_REQUEST['search']) != '') {
	$arFilter['NAME'] = '%' . trim($_REQUEST['search']) . '%';
}
$arSelect = array('ID', 'NAME');

$arNavParams = array(
    'nPageSize' => 30,
    'bShowAll' => 'Y',
    'iNumPage' => isset($_REQUEST['page']) ? $_REQUEST['page'] : 1
);

switch ($_REQUEST['property']) {
	case 'CUSTOMER':
		$dbItems = \Logistic\Contragent::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		break;
	case 'SENDER':
		$dbItems = \Logistic\Contragent::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		break;
	case 'SENDER_ADDRESS':
		if ($_REQUEST['SENDER'] > 0) {
			$arFilter['PROPERTY_CONTRAGENT'] = $_REQUEST['SENDER'];
			$dbItems = \Logistic\Address::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		}
		break;
	case 'SENDER_CONTACT':
		if ($_REQUEST['SENDER'] > 0) {
			$arFilter['PROPERTY_CONTRAGENT'] = $_REQUEST['SENDER'];
			$dbItems = \Logistic\Contact::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		}
		break;
	case 'RECIPIENT':
		if ($_REQUEST['SENDER'] > 0) {
			$dbItems = \Logistic\Contragent::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		}
		break;
	case 'RECIPIENT_ADDRESS':
		if ($_REQUEST['RECIPIENT'] > 0) {
			$arFilter['PROPERTY_CONTRAGENT'] = $_REQUEST['RECIPIENT'];
			$dbItems = \Logistic\Address::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		}
		break;
	case 'RECIPIENT_CONTACT':
		if ($_REQUEST['RECIPIENT'] > 0) {
			$arFilter['PROPERTY_CONTRAGENT'] = $_REQUEST['RECIPIENT'];
			$dbItems = \Logistic\Contact::getList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		}
		break;
	case 'THERMAL_RANGE':
		\Bitrix\Main\Loader::includeModule('iblock');
		$arFilter['IBLOCK_ID'] = THERMAL_RANGE_IBLOCK_ID;
		$dbItems = \CIBlockElement::GetList($arOrder, $arFilter, false, $arNavParams, $arSelect);
		break;
}

if (is_object($dbItems)) {
	if ($_REQUEST['property'] == 'SENDER_CONTACT' || $_REQUEST['property'] == 'RECIPIENT_CONTACT') {
		$result['results'][] = array(
			'id' => 0,
			'text' => 'Не выбрано'
		);
	}
	while ($item = $dbItems->Fetch()) {
		$result['results'][] = array(
			'id' => $item['ID'],
			'text' => $item['NAME']
		);
	}
	if ($dbItems->NavPageNomer < $dbItems->NavPageCount) {
		$result['pagination']['more'] = true;
	}
}
echo json_encode($result);
