<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$IBLOCK_ID = intval($arParams['IBLOCK_ID']);
if ($IBLOCK_ID <= 0) return false;

$ELEMENT_ID = intval($arParams['ELEMENT_ID']);

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('seoexpert.logistic');

$items = array();
$arResult = array(
	'ID' => 0
);

if ($ELEMENT_ID > 0) {
	$arResult = \Logistic\Application::fetchById($ELEMENT_ID);
	$arResult['PERMISSIONS'] = \Logistic\Application::getPermissions($ELEMENT_ID);
	if ($arResult['PERMISSIONS']['EDIT'] != 'Y' && $arParams['MODE'] == 'EDIT') LocalRedirect('/personal/applications/view/' . $ELEMENT_ID . '/');
	if ($arResult['PERMISSIONS']['VIEW'] != 'Y' && $arParams['MODE'] == 'REPEAT') LocalRedirect('/personal/applications/');
}

$arResult['LISTS'] = \Logistic\Application::fetchLists($ELEMENT_ID);

if (isset($_REQUEST['ACTION']) && $_REQUEST['ACTION'] == 'CANCEL') {
	global $APPLICATION;
	$APPLICATION->RestartBuffer();
	$ELEMENT_ID = $_REQUEST['ID'];
	if ($ELEMENT_ID <= 0) die();
	$perm = \Logistic\Application::getPermissions($ELEMENT_ID);
	if ($perm['CANCEL'] == 'Y') {
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array('STATUS' => \Bitrix\Main\Config\Option::get('seoexpert.logistic', 'application_cancelled_status')));
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array('CANCELLED' => 'Y'));
	}
	die();
}

if (isset($_REQUEST['SUBMIT_FORM']) && $_REQUEST['SUBMIT_FORM'] == 'Y') {
	global $APPLICATION;
	global $USER;
	$APPLICATION->RestartBuffer();
	$result = array();

	$props = array();
	$errors = array();

	$dbProps = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID));
	while ($prop = $dbProps->GetNext()) {
		if (in_array($prop['CODE'], array('DATE', 'CREATED_DATE', 'NUMBER', 'LOCKED'))) continue;
		if (in_array($prop['CODE'], array('KEEP_DRY', 'KEEP_AWAY_FROM_RADIATION', 'KEEP_AWAY_FROM_HEAT', 'FLAMMABLE', 'FRAGILE'))) {
			$props[$prop['CODE']] = isset($_REQUEST[$prop['CODE']]) && $_REQUEST[$prop['CODE']] == 'on' ? 'Y' : 'N';
		} elseif (isset($_REQUEST[$prop['CODE']]) && $_REQUEST[$prop['CODE']] != '') {
			$props[$prop['CODE']] = $_REQUEST[$prop['CODE']];
		} elseif ($prop['IS_REQUIRED'] == 'Y') {
			$errors[] = '– Поле &laquo;' . $prop['NAME'] . '&raquo; обязательно для заполнения';
		}
	}

	if (!empty($errors)) {
		$result = array('success' => false, 'html' => '<p style="margin-bottom: 5px;">Обнаружены ошибки:<br /><br /> ' . implode('<br />', $errors) . '</p>');
	} else {
		$el = new CIBlockElement;
		if ($ELEMENT_ID > 0 && $_REQUEST['MODE'] != 'REPEAT') {
			foreach ($props as $code => $value) {
				CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($code => $value));
			}
			$result = array('success' => true, 'html' => '<p>Заявка успешно обновлена.</p><p style="margin-bottom: 5px;"><a href="/personal/applications/" class="button">К списку заявок</a></p>');

		} else {
			$applicationNumber = \Logistic\Application::generateNumber();
			$props['NUMBER'] = $applicationNumber;
			$props['CREATED_DATE'] = date('d.m.Y H:i:s');
			$props['DATE'] = date('d.m.Y');
			$props['STATUS'] = 'Новый';

			$fields = array(
				'MODIFIED_BY' => $USER->GetID(),
				'IBLOCK_SECTION_ID' => false,
				'IBLOCK_ID' => $IBLOCK_ID,
				'PROPERTY_VALUES' => $props,
				'NAME' => $applicationNumber,
				'ACTIVE' => 'Y'
			);
			$ELEMENT_ID = $el->Add($fields);
			if ($ELEMENT_ID > 0) {
				$result = array('success' => true, 'html' => '<p>Заявка успешно добавлена.<br />Номер заявки: <strong>' . $applicationNumber . '</strong> от ' . date('d.m.Y') . '</p><p style="margin-bottom: 5px;"><a href="/personal/applications/" class="button">К списку заявок</a></p>');
			} else {
				$result = array('success' => false, 'html' => 'Произошла ошибка при добавлении заявки. Попробуйте повторить попытку позднее. ' . $el->LAST_ERROR);
			}
		}

		foreach ($arResult['LISTS'] as $listName => $v) {
			foreach ($_REQUEST[$listName]['ID'] as $key => $elementId) {
				if ($elementId == -1) continue;
				$props = array();
				foreach ($_REQUEST[$listName] as $propName => $propArr) {
					if ($propName != 'ID') {
						$props[$propName] = $propArr[$key];
					}
				}
				$props['LINK'] = $ELEMENT_ID;

				if ($elementId && $props['DELETE'] == 'Y') {
					CIBlockElement::Delete($elementId);
				} else {
					if ($elementId == 0) {
						$el = new CIBlockElement;
						$fields = array(
							'MODIFIED_BY' => $USER->GetID(),
							'IBLOCK_SECTION_ID' => false,
							'IBLOCK_ID' => $v['IBLOCK_ID'],
							'NAME' => 'К заявке ID = ' . $ELEMENT_ID,
							'ACTIVE' => 'Y'
						);
						$elementId = $el->Add($fields);
					}

					foreach ($props as $code => $value) {
						CIBlockElement::SetPropertyValuesEx($elementId, $v['IBLOCK_ID'], array($code => $value));
					}
				}
							
			}
		}

	}
	die(json_encode($result));
}

$this->IncludeComponentTemplate();
