<?php
define(APPLICATION_IBLOCK_ID, 16);
define(CONTRAGENT_IBLOCK_ID, 26);
define(ADDRESS_IBLOCK_ID, 20);
define(CONTACT_IBLOCK_ID, 19);
define(THERMAL_RANGE_IBLOCK_ID, 21);
define(PACKAGE_IBLOCK_ID, 22);
define(COMPATIBILITY_GROUP_IBLOCK_ID, 27);
define(NOMENCLATURE_IBLOCK_ID, 23);

\Bitrix\Main\EventManager::getInstance()->addEventHandler('main', 'OnBeforeProlog', function() {
	global $USER;
	if (!is_object($USER)) $USER = new CUser;
	if (true) { 	// !isset($_SESSION['CONTRAGENT'])) {
		$contragentData = array();
		$contactData = array();
		if ($USER->IsAuthorized()) {
			$userFields = CUser::GetByID($USER->GetID())->Fetch();
			if ($userFields['UF_CONTACT'] > 0) {
				\Bitrix\Main\Loader::includeModule('iblock');
				$dbContacts = CIBlockElement::GetList(array(), array('ACTIVE' => 'Y', 'ID' => $userFields['UF_CONTACT'], 'IBLOCK_ID' => CONTACT_IBLOCK_ID), false, false);
				if ($contact = $dbContacts->GetNextElement()) {
					$contactProperties = $contact->GetProperties();
					foreach ($contactProperties as $prop) {
						$contactData[$prop['CODE']] = $prop['VALUE'];
					}
					$contactData['NAME'] = $userFields['NAME'];
					if ($contactProperties['CONTRAGENT'] > 0) {
						$dbContragents = CIBlockElement::GetList(array(), array('ACTIVE' => 'Y', 'ID' => $contactProperties['CONTRAGENT'], 'IBLOCK_ID' => CONTRAGENT_IBLOCK_ID), false, false);
						if ($contragent = $dbContragents->GetNextElement()) {
							$contragentProperties = $contragent->GetProperties();
							foreach ($contragentProperties as $prop) {
								$contragentData[$prop['CODE']] = $prop['VALUE'];
							}
						}
					}
				}
			}
		}
		$GLOBALS['CONTRAGENT'] = $contragentData;
		$GLOBALS['CONTACT'] = $contactData;
		//$_SESSION['CONTRAGENT'] = $accountData;
	} else {
		$GLOBALS['CONTRAGENT'] = $_SESSION['CONTRAGENT'];
		$GLOBALS['CONTACT'] = $_SESSION['CONTACT'];
	}

	//echo '<pre>';
//var_dump($GLOBALS['CONTRAGENT']);
	//echo '</pre>';
});

\Bitrix\Main\EventManager::getInstance()->addEventHandler('iblock', 'OnBeforeIBlockElementAdd', function(&$arFields) {
	if ($arFields['IBLOCK_ID'] == APPLICATION_IBLOCK_ID) {
		// $arFields['PROPERTY_VALUES']['256']['n0']['VALUE'] = date('d.m.Y H:i:s');
		global $USER;
		// $arFields['PROPERTY_VALUES']['257']['n0']['VALUE'] = $USER->GetID();
	}
});
