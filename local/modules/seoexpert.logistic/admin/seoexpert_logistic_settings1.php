<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/seoexpert.logistic/include.php");
static $MODULE_ID = 'seoexpert.logistic';
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule($MODULE_ID);
global $USER;
if (!$USER->IsAdmin()) exit();

if (isset($_REQUEST['save'])) {
	// tab 1
	COption::SetOptionString($MODULE_ID, 'application_number_prefix', htmlspecialcharsbx($_REQUEST['application_number_prefix']));
	COption::SetOptionString($MODULE_ID, 'application_number_digits_count', htmlspecialcharsbx($_REQUEST['application_number_digits_count']));
	COption::SetOptionString($MODULE_ID, 'application_next_number', htmlspecialcharsbx($_REQUEST['application_next_number']));
	COption::SetOptionString($MODULE_ID, 'application_new_status', htmlspecialcharsbx($_REQUEST['application_new_status']));
	COption::SetOptionString($MODULE_ID, 'application_cancelled_status', htmlspecialcharsbx($_REQUEST['application_cancelled_status']));
	// tab 2
	COption::SetOptionString($MODULE_ID, 'pdf_contractor', htmlspecialcharsbx($_REQUEST['pdf_contractor']));
	// tab 3
	COption::SetOptionString($MODULE_ID, '1c_login', htmlspecialcharsbx($_REQUEST['1c_login']));
	COption::SetOptionString($MODULE_ID, '1c_password', htmlspecialcharsbx($_REQUEST['1c_password']));
	COption::SetOptionString($MODULE_ID, '1c_save_log', $_REQUEST['1c_save_log'] == 'on' ? 'Y' : 'N');
	COption::SetOptionString($MODULE_ID, '1c_save_xml', $_REQUEST['1c_save_xml'] == 'on' ? 'Y' : 'N');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aTabs = array(
	array("DIV" => "edit1", "TAB" => 'Заявки', "ICON" => "form_edit", "TITLE" => 'Заявки'),
	array("DIV" => "edit2", "TAB" => 'PDF', "ICON" => "form_edit", "TITLE" => 'PDF'),
	array("DIV" => "edit3", "TAB" => 'Обмен с 1С', "ICON" => "form_edit", "TITLE" => 'Обмен с 1С'),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
<?echo bitrix_sessid_post();?>
<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
<input type="hidden" name="save" value="Y" />
<?
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<tr class="adm-detail-field">
	<td width="40%">Префикс для заявок с сайта:</td>
	<td width="60%"><input type="text" name="application_number_prefix" size="20" maxlength="4" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, 'application_number_prefix'));?>"></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">Мин. число цифр в номере заявки:</td>
	<td width="60%"><input type="text" name="application_number_digits_count" size="20" maxlength="4" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, 'application_number_digits_count'));?>"></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">След. номер заявки:</td>
	<td width="60%"><input type="text" name="application_next_number" size="20" maxlength="8" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, 'application_next_number'));?>"></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">Статус новых заявок:</td>
	<td width="60%"><input type="text" name="application_new_status" size="20" maxlength="16" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, 'application_new_status'));?>"></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">Статус отмененных заявок:</td>
	<td width="60%"><input type="text" name="application_cancelled_status" size="20" maxlength="16" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, 'application_cancelled_status'));?>"></td>
</tr>
<?
$tabControl->EndTab();
$tabControl->BeginNextTab();
?>
<tr class="adm-detail-field">
	<td width="40%">Исполнитель:</td>
	<td width="60%"><input type="text" name="pdf_contractor" size="20" maxlength="255" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, 'pdf_contractor'));?>"></td>
</tr>
<?
$tabControl->EndTab();
$tabControl->BeginNextTab();
?>
<tr class="adm-detail-field">
	<td width="40%">Логин:</td>
	<td width="60%"><input type="text" name="1c_login" size="20" maxlength="16" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, '1c_login'));?>"></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">Пароль:</td>
	<td width="60%"><input type="password" name="1c_password" size="20" maxlength="16" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, '1c_password'));?>"></td>
</tr>
<tr class="adm-detail-field">
	<td width="100%" colspan="2"><div class="adm-info-message-wrap"><div class="adm-info-message"><p>В 1С в качестве адреса сайта необходимо указать: http://<?=$_SERVER['HTTP_HOST'];?>/local/1c/export1c.php</p></div></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">Сохранять детальный лог обмена</td>
	<td width="60%"><input type="checkbox" name="1c_save_log" <?if (COption::GetOptionString($MODULE_ID, '1c_save_log') == 'Y') echo 'checked="checked"';?>></td>
</tr>
<tr class="adm-detail-field">
	<td width="40%">Сохранять файл выгрузки, пришедший из 1С</td>
	<td width="60%"><input type="checkbox" name="1c_save_xml" <?if (COption::GetOptionString($MODULE_ID, '1c_save_xml') == 'Y') echo 'checked="checked"';?>></td>
</tr>
<?
$tabControl->EndTab();
$tabControl->Buttons(
  array(
    //"disabled"=>($POST_RIGHT<"W"),
    "back_url"=>"seoexpert_logistic_settings.php?lang=".LANG,
  )
);
$tabControl->End();
$tabControl->ShowWarnings("post_form", $message);
?>

<?
echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote();?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>