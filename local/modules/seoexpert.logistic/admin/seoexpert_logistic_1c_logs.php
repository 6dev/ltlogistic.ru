<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/seoexpert.logistic/include.php");
static $MODULE_ID = 'seoexpert.logistic';
IncludeModuleLangFile(__FILE__);

CModule::IncludeModule($MODULE_ID);
global $USER;
if (!$USER->IsAdmin()) exit();

if (isset($_REQUEST['ID']) || isset($_REQUEST['action'])) {
	$recordId = intval($_REQUEST['ID']);
	
	switch ($_REQUEST['action']) {
		case "abort":
			if ($recordId > 0) {
				\Logistic\Exchange1C::update($recordId, array('ABORT_IMPORT' => 'Y'));
				sleep(1);
			}
			break;
		case "delete":
			if ($recordId > 0) \Logistic\Exchange1C::delete($recordId);
			break;
		case "delete_all":
			$rsData = \Logistic\Exchange1C::getList();
			while ($arRes = $rsData->Fetch()) {
				\Logistic\Exchange1C::delete($arRes['ID']);
			}
			break;
	}
	LocalRedirect('seoexpert_logistic_1c_logs.php?lang='.LANG);
}

$tableId = "1c_logs";
$sortBy = isset($_REQUEST['by']) ? $_REQUEST['by'] : 'ID';
$sortOrder = isset($_REQUEST['order']) ? $_REQUEST['order'] : 'desc';

$sort = new CAdminSorting($tableId, $sortBy, $sortOrder);
$lAdmin = new CAdminList($tableId, $sort);
$rsData = \Logistic\Exchange1C::getList();
$rsData = new CAdminResult($rsData, $tableId);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint('Элементы'));
$APPLICATION->SetTitle('Элементы');

$lAdmin->AddHeaders(array(
	array(
		"id" => "START_DATE",
		"content" => "Начало",
		"sort" => "START_DATE",
		"default" => true,
	),
	array(
		"id" => "STOP_DATE",
		"content" => "Окончание",
		"sort" => "STOP_DATE",
		"default" => true,
	),
	array(
		"id" => "STATUS",
		"content" => "Статус",
		"sort" => "STATUS",
		"default" => true,
	),
	array(
		"id" => "STATUS",
		"content" => "Статус",
		"sort" => "STATUS",
		"default" => true,
	),
	array(
		"id" => "STATUS_TEXT",
		"content" => "Результат импорта",
		"sort" => "STATUS_TEXT",
		"default" => true,
	),
	array(
		"id" => "REMOTE_ADDR",
		"content" => "IP",
		"sort" => "REMOTE_ADDR",
		"default" => true,
	),
	array(
		"id" => "USER_AGENT",
		"content" => "User Agent",
		"sort" => "USER_AGENT",
		"default" => true,
	),
	array(
		"id" => "ID",
		"content" => "ID",
		"sort" => "ID",
		"default" => true,
	),
));

while ($arRes = $rsData->NavNext(true, "f_")) {
	//$arRes['ACTIVE_FROM'] = date('d.m.Y H:i:s', MakeTimeStamp($arRes['ACTIVE_FROM'], "YYYY-MM-DD HH:II:SS"));
	//$arRes['ACTIVE_TO'] = date('d.m.Y H:i:s', MakeTimeStamp($arRes['ACTIVE_TO'], "YYYY-MM-DD HH:II:SS"));

	$row =& $lAdmin->AddRow($f_ID, $arRes);

	$arActions = array();

	$arActions[] = array(
		"ICON" => "edit",
		"DEFAULT" => true,
		"TEXT" => 'Открыть лог файл',
		"ACTION" => $lAdmin->ActionRedirect('/upload/' . $MODULE_ID . '/' . basename($arRes['LOG_FILE']))
	);

	if (\Bitrix\Main\Config\Option::get($MODULE_ID, '1C_KEEP_XML', 'N') == 'Y' && file_exists($arRes['XML_FILE'])) {
		$arActions[] = array(
			"ICON" => "edit",
			"DEFAULT" => true,
			"TEXT" => 'Открыть XML файл',
			"ACTION" => $lAdmin->ActionRedirect('/upload/' . $MODULE_ID . '/' . basename($arRes['XML_FILE']))
		);
	}

	if ($arRes['STATUS'] == 'processing') {
		$arActions[] = array(
			"ICON" => "",
			"DEFAULT" => true,
			"TEXT" => 'Прервать импорт',
			"ACTION" => $lAdmin->ActionRedirect("seoexpert_logistic_1c_logs.php?ID=".$arRes['ID']."&action=abort")
		);
	}

	$arActions[] = array(
		"ICON" => "delete",
		"DEFAULT" => true,
		"TEXT" => 'Удалить из списка',
		"ACTION" => "if (confirm('Удалить?')) ". $lAdmin->ActionRedirect("seoexpert_logistic_1c_logs.php?ID=".$arRes['ID']."&action=delete")
	);
	
	$row->AddActions($arActions);
}

$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

//$aMenu[] = array("SEPARATOR" => "Y");
$aMenu = array();

$aContext = array(
		array(
			"ICON" => "btn_archive",
			"TEXT" => "Удалить все",
			"LINK" => "seoexpert_logistic_1c_logs.php?action=delete_all",
			"LINK_PARAM" => "onclick=\"if (!confirm('Будут безвозвратно удалены логи и xml-файлы всех обменов. Продолжить?')) return false\"",
			"TITLE" => "Удалить все"
		),
	);
$lAdmin->AddAdminContextMenu($aContext);

$context = new CAdminContextMenu($aMenu);
$context->Show();

$oFilter = new CAdminFilter($tableId . "_filter", array());
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
