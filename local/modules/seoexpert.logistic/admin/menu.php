<?php
$aMenu = array(
    'parent_menu' => 'global_menu_services',
    'section' => 'seoexpert_logistic',
    'sort' => 0,
    'module_id' => 'seoexpert.logistic',
    'text' => 'LT Logistic',
    'title'=> 'LT Logistic',
    'icon' => 'statistic_icon_online',
    'page_icon' => '',
    'items_id' => 'menu_ltlogistic_core',
    'items' => array(
		array(
            'text' => 'Параметры',
            'title' => 'Параметры',
            'url' => 'seoexpert_logistic_settings.php?lang=' . LANGUAGE_ID,
            'more_url' => array('seoexpert_logistic_settings.php?lang=' . LANGUAGE_ID),
			'items' => array()
		),
        array(
            'text' => 'Логи обмена с 1С',
            'title' => 'Логи обмена с 1С',
            'url' => 'seoexpert_logistic_1c_logs.php?lang=' . LANGUAGE_ID,
            'more_url' => array('seoexpert_logistic_1c_logs.php?lang=' . LANGUAGE_ID),
            'items' => array()
        ),
    )
);
return $aMenu;
