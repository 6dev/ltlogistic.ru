<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/seoexpert.logistic/include.php");
static $MODULE_ID = 'seoexpert.logistic';
IncludeModuleLangFile(__FILE__);
\Bitrix\Main\Loader::includeModule($MODULE_ID);
global $USER;
if (!$USER->IsAdmin()) exit();

$moduleSettings = array(
	array(
		'TITLE' => 'Личный кабинет',
		'SECTIONS' => array(
			array(
				'TITLE' => 'Вкладка "Оплатить услуги"',
				'SETTINGS' => array(
					array(
						'NAME' => 'PAY_TAB_TEXT',
						'TITLE' => 'Текст / HTML',
						'TYPE' => 'TEXTAREA',
					),
				),
			),
			
		),
	),
	array(
		'TITLE' => '',
		'SECTIONS' => array(
			
			
		),
	),
	array(
		'TITLE' => 'Обмен с 1С',
		'SECTIONS' => array(
			array(
				'TITLE' => 'Параметры обмена',
				'SETTINGS' => array(
					array(
						'NAME' => '1C_LOGIN',
						'TITLE' => 'Логин',
					),
					array(
						'NAME' => '1С_PASSWORD',
						'TITLE' => 'Пароль',
						'TYPE' => 'PASSWORD',
					),
					array(
						'TITLE' => 'В 1С в качестве адреса сайта необходимо указать: http://' . $_SERVER['HTTP_HOST'] . '/local/1c/export1c.php',
						'TYPE' => 'MESSAGE',
					),
					array(
						'NAME' => '1C_VERBOSE_LOGGING',
						'TITLE' => 'Вести детальный лог обмена',
						'TYPE' => 'CHECKBOX',
					),
					array(
						'NAME' => '1C_KEEP_XML',
						'TITLE' => 'Сохранять файл выгрузки, пришедший из 1С',
						'TYPE' => 'CHECKBOX',
					),
					array(
						'NAME' => '1C_USE_BASE64_ENC',
						'TITLE' => 'При обработке файла из 1С использовать base64-декодирование',
						'TYPE' => 'CHECKBOX',
					),
					array(
						'NAME' => '1C_STEP_INTERVAL',
						'TITLE' => 'Продолжительность шага обмена в секундах (0 - без ограничения)',
					),
				),
			),
			array(
				'TITLE' => 'Настройка импорта отдельных справочников и документов',
				'SETTINGS' => array(
					array(
						'NAME' => '1C_IMPORT_CONTRAGENTS',
						'TITLE' => 'Контрагенты',
						'TYPE' => 'CHECKBOX',
					),
					array(
						'NAME' => '1C_IMPORT_CONTACTS',
						'TITLE' => 'Контактные лица',
						'TYPE' => 'CHECKBOX',
					),
					

				),
			),
		),
	),
);

if (isset($_POST['save'])) {
	foreach ($moduleSettings as $settingsTab) {
		foreach ($settingsTab['SECTIONS'] as $settingsSection) {
			foreach ($settingsSection['SETTINGS'] as $settingsItem) {
				if ($settingsItem['TYPE'] == 'CHECKBOX') {
					COption::SetOptionString($MODULE_ID, $settingsItem['NAME'], $_REQUEST[$settingsItem['NAME']] == 'on' ? 'Y' : 'N');
				} else {
					COption::SetOptionString($MODULE_ID, $settingsItem['NAME'], $_REQUEST[$settingsItem['NAME']]);
				}
			}
		}
	}
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aTabs = array();
foreach ($moduleSettings as $k => $settingsTab) {
	$aTabs[] = array(
		'DIV' => 'edit' . $k,
		'TAB' => $settingsTab['TITLE'],
		'ICON' => 'form_edit',
		'TITLE' => $settingsTab['TITLE'],
	);
}
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form">
<?echo bitrix_sessid_post();?>
<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
<input type="hidden" name="save" value="Y" />

<?
$tabControl->Begin();
foreach ($moduleSettings as $settingsTab) {
	$tabControl->BeginNextTab();
	foreach ($settingsTab['SECTIONS'] as $settingsSection) {
		?>
		<tr class="heading"><td colspan="2"><?=$settingsSection['TITLE'];?></td></tr>
		<?
		foreach ($settingsSection['SETTINGS'] as $settingsItem) {
			?>
			<tr class="adm-detail-field">
				<?if ($settingsItem['TYPE'] == 'MESSAGE'):?>
					<td width="100%" colspan="2"><div class="adm-info-message-wrap"><div class="adm-info-message"><p><?=$settingsItem['TITLE'];?></p></div></div></td>
				<?else:?>
					<td width="40%"><?=$settingsItem['TITLE'];?><?if ($settingsItem['TYPE'] != 'CHECKBOX') echo ':';?></td>
					<td width="60%">
						<?if ($settingsItem['TYPE'] == 'TEXTAREA'):?>
							<textarea cols="80" rows="14" name="<?=$settingsItem['NAME'];?>"><?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, $settingsItem['NAME']));?></textarea>
						<?elseif ($settingsItem['TYPE'] == 'CHECKBOX'):?>
							<input type="checkbox" name="<?=$settingsItem['NAME'];?>" <?if (COption::GetOptionString($MODULE_ID, $settingsItem['NAME']) == 'Y'):?>checked="checked"<?endif;?>/>
						<?elseif ($settingsItem['TYPE'] == 'PASSWORD'):?>
							<input type="password" name="<?=$settingsItem['NAME'];?>" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, $settingsItem['NAME']));?>" size="30" />
						<?else:?>
							<input type="text" name="<?=$settingsItem['NAME'];?>" value="<?=htmlspecialcharsbx(COption::GetOptionString($MODULE_ID, $settingsItem['NAME']));?>" size="30" />
						<?endif;?>
					</td>
				<?endif;?>
			</tr>
			<?
		}
	}
	$tabControl->EndTab();
}
$tabControl->Buttons(
  array(
    // "disabled"=>($POST_RIGHT<"W"),
    "back_url" => "seoexpert_logistic_settings.php?lang=" . LANG,
  )
);
$tabControl->End();
$tabControl->ShowWarnings("post_form", $message);
echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?
echo EndNote();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
