<?php

class seoexpert_logistic extends CModule {

    var $MODULE_ID = 'seoexpert.logistic';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;
	
    public function seoexpert_logistic() {
        $this->MODULE_NAME = 'seoexpert.logistic';
        $this->MODULE_DESCRIPTION = 'seoexpert.logistic';
        include(__DIR__ . DIRECTORY_SEPARATOR . 'version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->PARTNER_NAME = '';
        $this->PARTNER_URI = '';
    }
	
    public function InstallDB() {
        global $DB, $DBType, $APPLICATION;   
    	return true;
    }
	
    public function InstallEvents() {
        return true;
    }
	
	public function InstallFiles() {
		$bCopyResult = CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . '/local/modules/' . $this->MODULE_ID . '/install/admin/', $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
		return $bCopyResult;
	}
	
    public function DoInstall() {
		global $APPLICATION;
		if (!$this->InstallDB() || !$this->InstallEvents() || !$this->InstallFiles()) {
			return false;
        }
        RegisterModule($this->MODULE_ID);
		$APPLICATION->IncludeAdminFile('Установка модуля ' . $this->MODULE_ID, $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/step.php');
		return true;
    }

    public function UnInstallDB() {
		return true;
    }
	
    public function UnInstallEvents() {
		return true;
    }
	
    public function UnInstallFiles() {
		return true;
    }
	
    public function DoUninstall() {
		global $DB, $DBType, $APPLICATION;
		if (!$this->UnInstallDB() || !$this->UnInstallEvents() || !$this->UnInstallFiles()) {
			return false;
        }
        UnRegisterModule($this->MODULE_ID);
		$APPLICATION->IncludeAdminFile('Удаление модуля ' . $this->MODULE_ID, $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/unstep.php');
		return true;
    }
}
