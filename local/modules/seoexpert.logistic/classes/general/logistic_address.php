<?php
namespace Logistic;
use \Bitrix\Main\Config\Option;

class Address {
	
	const MODULE_ID = 'seoexpert.logistic';

	public static function getList($arOrder, $arFilter, $w, $arNavParams, $arSelect) {
		if (!is_array($arFilter)) {
			$arFilter = array();
		}
		$arFilter['IBLOCK_ID'] = ADDRESS_IBLOCK_ID;
		\Bitrix\Main\Loader::includeModule('iblock');
		return \CIBlockElement::GetList($arOrder, $arFilter, false, $arNavParams, $arSelect);
	}

}
