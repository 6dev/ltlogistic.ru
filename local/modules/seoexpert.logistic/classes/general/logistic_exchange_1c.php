<?php
namespace Logistic;
use \Bitrix\Main\Config\Option;

class Exchange1C {

	const MODULE_ID = 'seoexpert.logistic';
	const TABLE_NAME = 'b_1c_exchange';

	private $ID = 0;
	private $LOG_FILE = '';
	private $XML_FILE = '';
	private $keepXML = false;
	private $verboseLogging = false;
	private $stepInterval = 0;

	private $IMPORT_MAP = array(


	);

	function __construct() {

		$this->verboseLogging = Option::get(self::MODULE_ID, '1C_VERBOSE_LOGGING', 'N') == 'Y' ? true : false;
		$this->keepXML = Option::get(self::MODULE_ID, '1C_KEEP_XML', 'N') == 'Y' ? true : false;
		$this->stepInterval = intval(Option::get(self::MODULE_ID, '1C_STEP_INTERVAL', 0));

		$DIR = $_SERVER["DOCUMENT_ROOT"] . '/upload/' . self::MODULE_ID;
		if (!file_exists($DIR) && !is_dir($DIR)) {
			mkdir($DIR);
		}

		if (!$this->checkAuth($_REQUEST['login'], $_REQUEST['password'])) {
            //return ($login == Option::get(self::MODULE_ID, '1C_LOGIN') && $password == Option::get(self::MODULE_ID, '1С_PASSWORD'));
			$this->abortImport('Неверный логин или пароль.');
		}

		if (!isset($_SESSION['NS'])) {
			$_SESSION['NS'] = array();
		}

		if (!isset($_SESSION['NS']['ID'])) {

			$prefix = date('Ymd') . '_' . date('His') . '_' . bin2hex(mcrypt_create_iv(12, MCRYPT_DEV_URANDOM));
			$this->LOG_FILE = $DIR . '/' . $prefix . '.log';
			$this->XML_FILE = $DIR . '/' . $prefix . '.xml';

			$fields = array(
				'START_DATE' => new \Bitrix\Main\Type\DateTime(),
				'STOP_DATE' => new \Bitrix\Main\Type\DateTime(),
				'LOG_FILE' => $this->LOG_FILE,
				'XML_FILE' => $this->XML_FILE,
				'STATUS' => 'processing',
				'STATUS_TEXT' => '',
				'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
				'USER_AGENT' => $_SERVER['HTTP_USER_AGENT']
			);

			$this->ID = $this->add($fields);
			$_SESSION['NS']['ID'] = $this->ID;
			$this->logEvent("Начало импорта.\n\$ID = " . $this->ID . ";\n\$_FILES = " . var_export($_FILES, true) . ";\n\$_REQUEST = " . var_export($_REQUEST, true) . ";\n\$_SERVER = " . var_export($_SERVER, true) . ";\n\$fields = " . var_export($fields, true));

			foreach ($fields as $k => $v) {
				$_SESSION['NS'][$k] = $v;
			}

		} else {
			$this->ID = $_SESSION['NS']['ID'];
			$this->XML_FILE = $_SESSION['NS']['XML_FILE'];
			$this->LOG_FILE = $_SESSION['NS']['LOG_FILE'];
		}

	}

	private function logEvent($s) {
		$s = date('[d.m.Y H:i:s]') . ' ' . $s . "\n";
		file_put_contents($this->LOG_FILE, \Bitrix\Main\Text\Encoding::convertEncoding($s, 'utf-8', 'windows-1251'), FILE_APPEND | LOCK_EX);
	}

	private function finishStep($statusText) {
		$status = 'progress';
		$this->logEvent('[' . $status . '] ' . $statusText);
		die($status . "\n" . $statusText);
	}

	private function abortImport($statusText) {
		$status = 'failure';
		$this->logEvent('[' . $status . '] ' . $statusText);
		self::update($this->ID, array('STATUS' => $status, 'STATUS_TEXT' => $statusText));
		die($status . "\n" . $statusText);
	}

	private function finishImport() {
		$status = 'success';
		$statusText = 'Обмен успешно завершен.';
		$this->logEvent('[' . $status . '] ' . $statusText);
		self::update($this->ID, array('STATUS' => $status, 'STATUS_TEXT' => $statusText));
		die($status . "\n" . $statusText);
	}

	private static function add($fields) {
		$connection = \Bitrix\Main\Application::getConnection();
		return $connection->add(self::TABLE_NAME, $fields);
	}

	static function delete($id) {
		$connection = \Bitrix\Main\Application::getConnection();
		$dbItem = self::getById($id);
		if ($arItem = $dbItem->Fetch()) {
			if ($arItem['LOG_FILE'] != '' && file_exists($arItem['LOG_FILE'])) {
				unlink($arItem['LOG_FILE']);
			}
			if ($arItem['XML_FILE'] != '' && file_exists($arItem['XML_FILE'])) {
				unlink($arItem['XML_FILE']);
			}
			$queryString = 'DELETE FROM `' . self::TABLE_NAME . '` WHERE `ID` = "' . $id . '" LIMIT 1;';
			return $connection->query($queryString);
		} else {
			return false;
		}
	}

	public static function update($id, $fields = array()) {
		$connection = \Bitrix\Main\Application::getConnection();
		$sqlHelper = $connection->getSqlHelper();
		global $DB;
		$id = (int)$id;
		if ($id <= 0) {
			return false;
		}
		$updateString = $DB->PrepareUpdate(self::TABLE_NAME, $fields);
		if ($updateString != "") {
			if (!$connection->query("UPDATE `" . self::TABLE_NAME . "` SET " . $updateString . " WHERE ID=" . $id)) {
				return false; 
			}
		}
		return true; 
	}

	static function getList() {
		$connection = \Bitrix\Main\Application::getConnection();
		$queryString = 'SELECT * FROM `' . self::TABLE_NAME . '` ORDER BY ID DESC';
		return $connection->query($queryString);
 	}

	static function getById($id) {
		$connection = \Bitrix\Main\Application::getConnection();
		$queryString = 'SELECT * FROM `' . self::TABLE_NAME . '` WHERE `ID` = "' . intval($id) . '";';
		return $connection->query($queryString);
 	}

 	private function checkAuth($login, $password) {
 		return ($login == Option::get(self::MODULE_ID, '1C_LOGIN') && $password == Option::get(self::MODULE_ID, '1С_PASSWORD'));
 	}

	function plcb($matches) {
		return ' ' . $matches[0];
	}

 	public static function translitCode($s) {
		$s = preg_replace_callback('![A-Z|А-Я]!u', 'plcb', $s);
		$arParams = array("replace_space" => "_", "replace_other" => "_", 'change_case' => 'U', 'max_len' => 50);
		return \Cutil::translit($s, 'ru', $arParams);
	}

	private function importToElement($IBLOCK_ID, $XML_ID, $arFields, $arProperties) {

		if ($XML_ID == '') return false;

		// update or add element
		$el = new \CIBlockElement;
		$dbItems = \CIBlockElement::GetList([], ['XML_ID' => $XML_ID, 'IBLOCK_ID' => $IBLOCK_ID], false, false, ['ID', 'XML_ID']);
		if ($item = $dbItems->GetNext()) {
			$ELEMENT_ID = $item['ID'];
			if (!$el->Update($ELEMENT_ID, $arFields)) {
				$this->abortImport($el->LAST_ERROR);
			}
			foreach ($arProperties as $code => $value) {
				\CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($code => $value));
			}
			$this->logEvent('Обновлен элемент ID=' . $ELEMENT_ID . '; XML_ID=' . $XML_ID . ', IBLOCK_ID=' . $IBLOCK_ID);
		} else {
			$arFields['IBLOCK_ID'] = $IBLOCK_ID;
			$arFields['XML_ID'] = $XML_ID;
			$ELEMENT_ID = $el->Add($arFields);
			if ($ELEMENT_ID <= 0) {
				$this->abortImport($el->LAST_ERROR);
			}
			foreach ($arProperties as $code => $value) {
				\CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($code => $value));
			}
			$this->logEvent('Добавлен элемент ID=' . $ELEMENT_ID . ', XML_ID=' . $XML_ID . ', IBLOCK_ID=' . $IBLOCK_ID);
		}
		return $ELEMENT_ID;
	}

	private function getIBlockProperties($IBLOCK_ID) {
		$IBLOCK_PROPS = array();
		$dbProps = \CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID));
		while ($prop = $dbProps->GetNext()) {
			if ($prop['CODE'] != '' && !isset($IBLOCK_PROPS[$prop['CODE']])) {
				$IBLOCK_PROPS[$prop['CODE']] = $prop;
			}
		}
		return $IBLOCK_PROPS;
	}

	private function prepareProperties($IBLOCK_ID, &$IBLOCK_PROPS, $tmp, $mapFields, &$arFields, &$arProperties) {
		foreach ($mapFields as $mapField) {
			if (isset($tmp[$mapField['XML_NODE']])) {
				if (isset($mapField['BX_FIELD'])) {
					$arFields[$mapField['BX_FIELD']] = $tmp[$mapField['XML_NODE']];
				}
				if (isset($mapField['BX_PROPERTY'])) {
					$p = $mapField['BX_PROPERTY'];
					$v = $tmp[$mapField['XML_NODE']];
					if (!isset($IBLOCK_PROPS[$p])/* && $map['CREATE_BX_PROPERTIES'] == 'Y'*/) {
						$propFields = array(
							'NAME' => $mapField['XML_NODE'],
							'CODE' => $p,
							'ACTIVE' => 'Y',
							'SORT' => '500',
							'PROPERTY_TYPE' => 'S',
							'IBLOCK_ID' => $IBLOCK_ID,
						);
						$ibp = new \CIBlockProperty;
						if ($ibp->Add($propFields)) {
							$IBLOCK_PROPS[$p] = $propFields;
						}
					}
					switch ($IBLOCK_PROPS[$p]['PROPERTY_TYPE']) {
						case 'E':
							$LINK_IBLOCK_ID = $IBLOCK_PROPS[$p]['LINK_IBLOCK_ID'];
							if ($XML_ID != $v) {
								$filter = array('XML_ID' => $v, 'IBLOCK_ID' => $LINK_IBLOCK_ID);
								if ($element = \CIBlockElement::GetList(array(), $filter, false, false, array('ID'))->Fetch()) {
									$v = $element['ID'];
								} else {
									$v = '';
								}
							} else {
								$v = '';
							}
							break;
						case 'N':
							$v = preg_replace("/[^0-9.,]/", "", $v);
							$v = str_replace(',', '.', $v);
							$v = floatval($v);
							break;
						case 'S':
							if (isset($IBLOCK_PROPS[$p]['USER_TYPE']) && $IBLOCK_PROPS[$p]['USER_TYPE'] == 'DateTime') {
								$v = date('d.m.Y H:i', strtotime($v));
							} elseif ($v == 'true') {
								$v = 'Y';
							} elseif ($v == 'false') {
								$v = 'N';
							} 
							break;
						default:
							//
							break;
					}
					$arProperties[$p] = $v;
				}
			}
		}
		$arFields['ACTIVE'] = 'Y';
	}

	private function importToIBlock($map) {

		$this->logEvent('Начало импорта справочника ' . $map['XML_ROOT']);
		$counter = 0;

		$IBLOCK_ID = intval($map['IBLOCK_ID']);
		if ($IBLOCK_ID <= 0) return false;

		// read properties list
		$IBLOCK_PROPS = $this->getIBlockProperties($IBLOCK_ID);
		foreach ($map['FIELDS'] as $k => $v) {
			if (isset($v['BX_PROPERTY']) && $v['BX_PROPERTY'] == '' && $v['XML_NODE'] != '') {
				$map['FIELDS'][$k]['BX_PROPERTY'] = self::translitCode($v['XML_NODE']);
			}
		}

		$obXMLFile = new \CIBlockXMLFile;
		// iterate xml tree
		$obRoot = $obXMLFile->GetList(
			array('ID' => 'DESC', 'LEFT_MARGIN' => 'ASC'),
			array('NAME' => $map['XML_ROOT']),
			array('ID', 'DEPTH_LEVEL', 'ATTRIBUTES')
		);
		if ($root = $obRoot->Fetch()) {
			$attr = unserialize($root['ATTRIBUTES']);

			//todo ["СодержитТолькоИзменения"]=>string(5) "false"

			$obChildren = $obXMLFile->GetList(
				array('LEFT_MARGIN' => 'ASC'),
				array('PARENT_ID' => $root['ID']),
				array('ID', 'NAME')
			);
			$itemsCount = $obXMLFile->GetCountItemsWithParent( $root['ID']);
			while ($child = $obChildren->Fetch()) {
				$counter++;

				if (($this->stepInterval > 0) && (microtime(true) - $_SESSION['NS']['SCRIPT_START'] > $this->stepInterval)) {
					$this->finishStep('Обработано ' . $counter . ' из ' .  $itemsCount . ' элементов.');
				}
				if ($_SESSION['NS']['COUNTER'] > 0 && $counter <= $_SESSION['NS']['COUNTER']) {
					continue;
				}
				$_SESSION['NS']['COUNTER'] = $counter;

				$tmp = array();
				$obNodes = $obXMLFile->GetList(
					array('LEFT_MARGIN' => 'ASC'),
					array('PARENT_ID' => $child['ID']),
					array('ID', 'NAME', 'VALUE')
				);
				while ($node = $obNodes->Fetch()) {
					$nodeName = trim($node['NAME']);
					$hasChildren = false;
					if ($nodeName != '') {
						// check if the node has children
						if (is_null($node['VALUE'])) {
							$obSubNodes = $obXMLFile->GetList(
								array('LEFT_MARGIN' => 'ASC'),
								array('PARENT_ID' => $node['ID']),
								array('ID', 'NAME', 'VALUE')
							);
							// TODO: rewrite as recursion if double nesting level is not enough
							while ($subNode = $obSubNodes->Fetch()) {
								$subNodeName = trim($subNode['NAME']);
								$hasChildren = true;
								if (!is_null($subNode['VALUE'])) {
									$tmp[$nodeName . '.' . $subNodeName] = trim(html_entity_decode($subNode['VALUE']));
								} else {
									$obSubSubNodes = $obXMLFile->GetList(
										array('LEFT_MARGIN' => 'ASC'),
										array('PARENT_ID' => $subNode['ID']),
										array('ID', 'NAME', 'VALUE')
									);
									$subTmp = array();
									while ($subSubNode = $obSubSubNodes->Fetch()) {
										$subSubNodeName = trim($subSubNode['NAME']);
										if (!is_null($subSubNodeName['VALUE'])) {
											$subTmp[$subSubNodeName] = trim(html_entity_decode($subSubNode['VALUE']));
										}
									}
									$tmp[$nodeName][] = $subTmp;
								}
							}
						}
					}
					if (!$hasChildren) {
						$tmp[$nodeName] = trim(html_entity_decode($node['VALUE']));
					}
				}
				$XML_ID = isset($tmp['Ид']) ? $tmp['Ид'] : '';
				if ($XML_ID == '') continue;
				
				// prepare $arFields and $arProperties
				$arFields = array();
				$arProperties = array();
				$this->prepareProperties($IBLOCK_ID, $IBLOCK_PROPS, $tmp, $map['FIELDS'], $arFields, $arProperties);

				if ($arFields['NAME'] != '') {

					if ($counter % 10 == 0) {
						$ex = \Gorvod\Exchange1C::getById($this->ID)->Fetch();
						if ($ex['ABORT_IMPORT'] == 'Y') {
							$this->abortImport('Обмен прерван пользователем');
							die();
						}
					}

					$ELEMENT_ID = $this->importToElement($IBLOCK_ID, $XML_ID, $arFields, $arProperties);
					$LINKED_ELEMENT_ID = $ELEMENT_ID;
					foreach ($map['FIELDS'] as $mapField) {

						$xmlNode = $mapField['XML_NODE'];
						if (isset($tmp[$xmlNode]) && is_array($tmp[$xmlNode]) && isset($mapField['BX_LINKED_ELEMENTS']) && is_array($mapField['BX_LINKED_ELEMENTS'])) {

							$arFields = array();
							$arProperties = array();

							$BX_LINK_IBLOCK_ID = $mapField['BX_LINKED_ELEMENTS']['IBLOCK_ID'];
							$BX_LINK_IBLOCK_PROPS = $this->getIBlockProperties($BX_LINK_IBLOCK_ID);

							$BX_LINK_PROPERTY = $mapField['BX_LINKED_ELEMENTS']['BX_LINK_PROPERTY'];
							if (!isset($BX_LINK_IBLOCK_PROPS[$BX_LINK_PROPERTY]) && $BX_LINK_PROPERTY != '') {
								$propFields = array(
									'NAME' => '',
									'CODE' => $BX_LINK_PROPERTY,
									'ACTIVE' => 'Y',
									'SORT' => '500',
									'PROPERTY_TYPE' => 'S',
									'IBLOCK_ID' => $BX_LINK_IBLOCK_ID,
								);
								$ibp = new \CIBlockProperty;
								if ($ibp->Add($propFields)) {
									$BX_LINK_IBLOCK_PROPS[$BX_LINK_PROPERTY] = $propFields;
								}
							}

							foreach ($tmp[$xmlNode] as $bxLinkTmp) {
								$this->prepareProperties($BX_LINK_IBLOCK_ID, $BX_LINK_IBLOCK_PROPS, $bxLinkTmp, $mapField['BX_LINKED_ELEMENTS']['FIELDS'], $arFields, $arProperties);

								if ($BX_LINK_PROPERTY != '') {
									$arProperties[$BX_LINK_PROPERTY] = $LINKED_ELEMENT_ID;
								}

								$BX_LINK_XML_ID = isset($bxLinkTmp['Ид']) ? $bxLinkTmp['Ид'] : '';
								if ($BX_LINK_XML_ID == '') continue;

								if ($arFields['NAME'] != '') {
									$BX_LINK_ELEMENT_ID = $this->importToElement($BX_LINK_IBLOCK_ID, $BX_LINK_XML_ID, $arFields, $arProperties);
								}
							}

						}
					}
				}
			}

		}
		unset($_SESSION['NS']['COUNTER']);
		$_SESSION['NS']['IMPORTED_SECTIONS'][] = $map['ID'];
		$this->finishStep('Импорт справочника ' . $map['XML_ROOT'] . ' завершен.');
		unset($_SESSION['NS']['CURRENT_SECTION']);
	}

 	public function processImport() {

 		if ($this->ID <= 0) {
 			$this->abortImport('Некорректный ID обмена.');
 		}

  		ini_set('max_execution_time', 600);
 		set_time_limit(600);
 		\Bitrix\Main\Loader::includeModule('iblock');
 		
 		$_SESSION['NS']['STEP'] = isset($_SESSION['NS']['STEP']) ? intval($_SESSION['NS']['STEP']) : 0;
 		$_SESSION['NS']['SCRIPT_START'] = microtime(true);

		if (!empty($_FILES) && !file_exists($this->XML_FILE)) {
			$key = array_keys($_FILES)[0];
			if ($_FILES[$key]['error'] == UPLOAD_ERR_OK) {
				$file = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . date('YmdHis') . rand(1000,9999) . '.' . pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);
				move_uploaded_file($_FILES[$key]['tmp_name'], $file);
				if (file_exists($file) && filesize($file) > 0) {
					$ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
			 		if ($ext == 'XML') {
			 			copy($file, $this->XML_FILE);
			 		} elseif ($ext == 'ZIP') {
			 			if (Option::get(self::MODULE_ID, '1C_USE_BASE64_ENC', 'Y') == 'Y') {
			 				file_put_contents($file, base64_decode(file_get_contents($file)));
			 			}
			 			$zip = new \ZipArchive;
						if ($zip->open($file)) {
							$xmlFile = $zip->getNameIndex(0);
							$zip->close();
							file_put_contents($this->XML_FILE, file_get_contents("zip://$file#$xmlFile"));
						} else {
							$this->abortImport('Не удалось распаковать архив.');
						}
			 		} else {
			 			return $this->abortImport('Данный формат файла не поддерживается.');
			 		}

				}
				$this->finishStep('Файл успешно загружен.');
				unlink($file);
			}
		}

		if ($_SESSION['NS']['STEP'] < 1) {
	 		$_SESSION['NS']['STEP'] = 1;
			$obXMLFile = new \CIBlockXMLFile;
			$obXMLFile->DropTemporaryTables();
			$this->logEvent('Временные таблицы удалены.');
			$obXMLFile->CreateTemporaryTables();
			$this->logEvent('Временные таблицы созданы.');
			$this->finishStep('Временные таблицы созданы.');
		}

		if ($_SESSION['NS']['STEP'] < 2) {
			if ($fp = fopen($this->XML_FILE, "rb")) {
				$_SESSION['NS']['STEP'] = 2;
				$obXMLFile = new \CIBlockXMLFile;
				$obXMLFile->ReadXMLToDatabase($fp, $_SESSION, 0);
				fclose($fp);
				$this->finishStep('Файл прочитан в базу данных.');
			} else {
				$this->abortImport('Ошибка открытия файла.');
			}
		}

		if ($_SESSION['NS']['STEP'] <= 3) {
			$_SESSION['NS']['STEP'] = 3;
			foreach ($this->IMPORT_MAP as $map) {
				if (!isset($map['ID']) || $map['ID'] == '') {
					continue;
				}
				if (Option::get(self::MODULE_ID, '1C_IMPORT_' . $map['ID'], 'N') == 'Y') {
					if (in_array($map['ID'], $_SESSION['NS']['IMPORTED_SECTIONS'])) {
						continue;
					}
					$_SESSION['NS']['CURRENT_SECTION'] = $map['ID'];
					$this->importToIBlock($map);
					unset($_SESSION['NS']['COUNTER']);
				}
			}
			return $this->finishImport();
		}

 	}
}
