<?php
namespace Logistic;
use \Bitrix\Main\Config\Option;

class Exchange1C {

	const MODULE_ID = 'seoexpert.logistic';
	const TABLE_NAME = 'b_1c_exchange';

	private $isDebugEnabled = false;
	private $LOG_FILE = '';
	private $doSaveXml = false;
	private $XML_FILE = '';
	private $exchangeId = 0;
	private $obXMLFile;

	private $IMPORT_MAP = array(
		// Контрагенты
		'CONTRAGENT' => array(
			'XML_ROOT' => 'Контрагенты',
			'IBLOCK_ID' => '26',
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ОфициальноеНаименование',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ИНН',
					'BX_PROPERTY' => 'INN'
				),
				array(
					'XML_NODE' => 'КПП',
					'BX_PROPERTY' => 'KPP'
				),
				array(
					'XML_NODE' => 'КодПоОКПО',
					'BX_PROPERTY' => 'KOD_PO_OKPO'
				),
				array(
					'XML_NODE' => 'Префикс',
					'BX_PROPERTY' => ''
				),	
				array(
					'XML_NODE' => 'Архив',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Группа',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ГоловнойКонтрагент',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ОбщийДоступОтравитель',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ОбщийДоступПолучатель',
					'BX_PROPERTY' => ''
				),
			),
		),
		// АдресаКонтрагентов
		'ADDRESS' => array(
			'XML_ROOT' => 'АдресаКонтрагентов',
			'IBLOCK_ID' => ADDRESS_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Котрагент',
					'BX_PROPERTY' => 'KONTRAGENT'
				),
			),
		),
		// КонтактныеЛица
		'CONTACT' => array(
			'XML_ROOT' => 'КонтактныеЛица',
			'IBLOCK_ID' => CONTACT_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ИдКонтрагента',
					'BX_PROPERTY' => 'KONTRAGENT'
				),
				array(
					'XML_NODE' => 'ТолькоПользователь',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Телефон',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Email',
					'BX_PROPERTY' => ''
				),
			),
		),
		// ТемпературныеРежимы
		'THERMAL_RANGE' => array(
			'XML_ROOT' => 'ТемпературныеРежимы',
			'IBLOCK_ID' => THERMAL_RANGE_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
			),
		),
		// Упаковки
		'PACKAGE' => array(
			'XML_ROOT' => 'Упаковки',
			'IBLOCK_ID' => PACKAGE_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
			),
		),
		// ГруппыСовместимости
		'COMPATIBILITY_GROUP' => array(
			'XML_ROOT' => 'ГруппыСовместимости',
			'IBLOCK_ID' => COMPATIBILITY_GROUP_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
			),
		),
		// СправочникНоменклатура
		'NOMENCLATURE' => array(
			'XML_ROOT' => 'СправочникНоменклатура',
			'IBLOCK_ID' => NOMENCLATURE_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'Y',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Наименование',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Услуга',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Владелец',
					'BX_PROPERTY' => ''
				),
			),
		),
		// Заявки
		'APPLICATION' => array(
			'XML_ROOT' => 'Заявки',
			'IBLOCK_ID' => APPLICATION_IBLOCK_ID,
			'CREATE_BX_PROPERTIES' => 'N',
			'FIELDS' => array(
				array(
					'XML_NODE' => 'Номер',
					'BX_FIELD' => 'NAME',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Дата',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ТипФайла',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Состояние',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Отменена',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Заказчик',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Заблокировать',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ОписаниеГруза',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ВесБрутто',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ВесНетто',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Объем',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'КоличествоМест',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'КоличествоНегабаритныхМест1',
					'BX_PROPERTY' => 'KOL_VO_GRUZOVYKH_NEGABARITNYKH_MEST'
				),
				array(
					'XML_NODE' => 'КоличествоНегабаритныхМест2',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ПризнакиНегабаритности',
					'BX_PROPERTY' => 'PRIZNAK_NEGABARITNOSTI'
				),
				array(
					'XML_NODE' => 'ОценочнаяСтоимость',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'температурныйРежим',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'БеречьОтВлаги',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'БеречьОтИзлучения',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'БеречьОтНагрева',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Огнеопасный',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Хрупкий',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'ДопустимыеПотери',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'КомментарийКГрузу',
					'BX_PROPERTY' => ''
				),
				array(
					'XML_NODE' => 'Отправитель.Контрагент',
					'BX_PROPERTY' => 'OTPRAVITEL'
				),
				array(
					'XML_NODE' => 'Отправитель.Адрес',
					'BX_PROPERTY' => 'ADRES_OTPRAVITELYA'
				),
				array(
					'XML_NODE' => 'Отправитель.ВремяНачала',
					'BX_PROPERTY' => 'DATA_OTPRAVLENIYA'
				),
				array(
					'XML_NODE' => 'Отправитель.ВремяОкончания',
					'BX_PROPERTY' => 'DATA_OTPRAVLENIYA_DO'
				),
				array(
					'XML_NODE' => 'Получатель.Контрагент',
					'BX_PROPERTY' => 'POLUCHATEL'
				),
				array(
					'XML_NODE' => 'Получатель.Адрес',
					'BX_PROPERTY' => 'ADRES_POLUCHATELYA'
				),
				array(
					'XML_NODE' => 'Получатель.ВремяНачала',
					'BX_PROPERTY' => 'DATA_DOSTAVKI'
				),
				array(
					'XML_NODE' => 'Получатель.ВремяОкончания',
					'BX_PROPERTY' => 'DATA_DOSTAVKI_DO'
				),
			),
		),
	);

	function __construct($login = '', $password = '') {
		$this->isDebugEnabled = Option::get(self::MODULE_ID, '1c_save_log', 'N') == 'Y' ? true : false;
		$this->doSaveXml = Option::get(self::MODULE_ID, '1c_save_xml', 'N') == 'Y' ? true : false;
		$DIR = $_SERVER["DOCUMENT_ROOT"] . '/upload/' . self::MODULE_ID;
		if (!file_exists($DIR) && !is_dir($DIR)) {
			mkdir($DIR);
		}
		$prefix = date('Ymd') . '_' . date('His') . '_' . bin2hex(mcrypt_create_iv(12, MCRYPT_DEV_URANDOM));
		$this->LOG_FILE = $DIR . '/' . $prefix . '.log';
		$this->XML_FILE = $DIR . '/' . $prefix . '.xml';

		$fields = array(
			'START_DATE' => new \Bitrix\Main\Type\DateTime(),
			'STOP_DATE' => new \Bitrix\Main\Type\DateTime(),
			'LOG_FILE' => $this->isDebugEnabled ? $this->LOG_FILE : '',
			'XML_FILE' => $this->doSaveXml ? $this->XML_FILE : '',
			'STATUS' => 'processing',
			'STATUS_TEXT' => '',
			'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
			'USER_AGENT' => $_SERVER['HTTP_USER_AGENT']
		);
		$this->exchangeId = $this->add($fields);

		$this->logEvent("Начало импорта.\n\$ID = " . $this->exchangeId . ";\n\$_REQUEST = " . var_export($_REQUEST, true) . ";\n\$_SERVER = " . var_export($_SERVER, true) . ";\n\$fields = " . var_export($fields, true));

		if (!$this->checkAuth($login, $password)) {
			$this->abortImport('Неверный логин или пароль.');
		}
	}

	private function logEvent($s) {
		if ($this->isDebugEnabled) {
			$s = date('[d.m.Y H:i:s]') . ' ' . $s . "\n";
			file_put_contents($this->LOG_FILE, \Bitrix\Main\Text\Encoding::convertEncoding($s, 'utf-8', 'windows-1251'), FILE_APPEND | LOCK_EX);
		}
	}

	private function abortImport($statusText) {
		$status = 'failure';
		$this->logEvent($statusText);
		self::update($this->exchangeId, array('STATUS' => $status, 'STATUS_TEXT' => $statusText));
		die($status . "\n" . $statusText);
	}

	private function finishImport() {
		$status = 'success';
		$statusText = 'Импорт успешно завершен.';
		$this->logEvent($statusText);
		self::update($this->exchangeId, array('STATUS' => $status, 'STATUS_TEXT' => $statusText));
		echo $status . "\n" . $statusText;
	}

	private static function add($fields) {
		$connection = \Bitrix\Main\Application::getConnection();
		return $connection->add(self::TABLE_NAME, $fields);
	}

	static function delete($id) {
		$connection = \Bitrix\Main\Application::getConnection();
		$dbItem = self::getById($id);
		if ($arItem = $dbItem->Fetch()) {
			if ($arItem['LOG_FILE'] != '' && file_exists($arItem['LOG_FILE'])) {
				unlink($arItem['LOG_FILE']);
			}
			if ($arItem['XML_FILE'] != '' && file_exists($arItem['XML_FILE'])) {
				unlink($arItem['XML_FILE']);
			}
			$queryString = 'DELETE FROM `' . self::TABLE_NAME . '` WHERE `ID` = "' . $id . '" LIMIT 1;';
			return $connection->query($queryString);
		} else {
			return false;
		}
	}

	private static function update($id, $fields = array()) {
		$connection = \Bitrix\Main\Application::getConnection();
		$sqlHelper = $connection->getSqlHelper();
		global $DB;
		$id = (int)$id;
		if ($id <= 0) {
			return false;
		}
		$updateString = $DB->PrepareUpdate(self::TABLE_NAME, $fields);
		if ($updateString != "") {
			if (!$connection->query("UPDATE `" . self::TABLE_NAME . "` SET " . $updateString . " WHERE ID=" . $id)) {
				return false; 
			}
		}
		return true; 
	}

	static function getList() {
		$connection = \Bitrix\Main\Application::getConnection();
		$queryString = 'SELECT * FROM `' . self::TABLE_NAME . '` ORDER BY ID DESC';
		return $connection->query($queryString);
 	}

	static function getById($id) {
		$connection = \Bitrix\Main\Application::getConnection();
		$queryString = 'SELECT * FROM `' . self::TABLE_NAME . '` WHERE `ID` = "' . intval($id) . '";';
		return $connection->query($queryString);
 	}

 	private function checkAuth($login, $password) {
 		return ($login == Option::get(self::MODULE_ID, '1c_login') && $password == Option::get(self::MODULE_ID, '1c_password'));
 	}

	function plcb($matches) {
		return ' '.$matches[0];
	}

 	public static function translitCode($s) {
		$name1 = preg_replace_callback('![A-Z|А-Я]!u', 'plcb', $s);

		$arParams = array("replace_space"=>"_","replace_other"=>"_", 'change_case' => 'U', 'max_len' => 50);
		$tempCode = \Cutil::translit($name1,"ru",$arParams);
		return $tempCode;
	}

	private function importToIBlock($map) {
		$this->logEvent('Начало импорта справочника ' . $map['XML_ROOT']);

		$IBLOCK_ID = intval($map['IBLOCK_ID']);
		if ($IBLOCK_ID <= 0) return false;

		// read properties list
		$IBLOCK_PROPS = array();
		$dbProps = \CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID));
		while ($prop = $dbProps->GetNext()) {
			if ($prop['CODE'] != '' && !isset($IBLOCK_PROPS[$prop['CODE']])) {
				$IBLOCK_PROPS[$prop['CODE']] = $prop;
			}
		}
		foreach ($map['FIELDS'] as $k => $v) {
			if (isset($v['BX_PROPERTY']) && $v['BX_PROPERTY'] == '' && $v['XML_NODE'] != '') {
				$map['FIELDS'][$k]['BX_PROPERTY'] = self::translitCode($v['XML_NODE']);
			}
		}

		// iterate xml tree
		$obRoot = $this->obXMLFile->GetList(
			array('ID' => 'DESC', 'LEFT_MARGIN' => 'ASC'),
			array('NAME' => $map['XML_ROOT']),
			array('ID', 'DEPTH_LEVEL', 'ATTRIBUTES')
		);
		if ($root = $obRoot->Fetch()) {
			$attr = unserialize($root['ATTRIBUTES']);

			//todo ["СодержитТолькоИзменения"]=>string(5) "false"

			$obChildren = $this->obXMLFile->GetList(
				array('LEFT_MARGIN' => 'ASC'),
				array('PARENT_ID' => $root['ID']),
				array('ID', 'NAME')
			);
			while ($child = $obChildren->Fetch()) {
				$tmp = array();
				$obNodes = $this->obXMLFile->GetList(
					array('LEFT_MARGIN' => 'ASC'),
					array('PARENT_ID' => $child['ID']),
					array('ID', 'NAME', 'VALUE')
				);
				while ($node = $obNodes->Fetch()) {
					$nodeName = trim($node['NAME']);
					$hasChildren = false;
					if ($nodeName != '') {
						// check if the node has children
						if (is_null($node['VALUE'])) {
							$obSubNodes = $this->obXMLFile->GetList(
								array('LEFT_MARGIN' => 'ASC'),
								array('PARENT_ID' => $node['ID']),
								array('ID', 'NAME', 'VALUE')
							);
							// TODO: rewrite as recursion if double nesting level is not enough
							while ($subNode = $obSubNodes->Fetch()) {
								$subNodeName = trim($subNode['NAME']);
								$hasChildren = true;
								if (!is_null($subNode['VALUE'])) {
									$tmp[$nodeName . '.' . $subNodeName] = trim(html_entity_decode($subNode['VALUE']));
								} else {
									$obSubSubNodes = $this->obXMLFile->GetList(
										array('LEFT_MARGIN' => 'ASC'),
										array('PARENT_ID' => $subNode['ID']),
										array('ID', 'NAME', 'VALUE')
									);
									while ($subSubNode = $obSubSubNodes->Fetch()) {
										$subSubNodeName = trim($subSubNode['NAME']);
										if (!is_null($subSubNodeName['VALUE'])) {
											$tmp[$nodeName . '.' . $subNodeName . '.' . $subSubNodeName] = trim(html_entity_decode($subSubNode['VALUE']));
										}
									}
								}
							}
							// TODO
						}
					}
					if (!$hasChildren) {
						$tmp[$nodeName] = trim(html_entity_decode($node['VALUE']));
					}
				}
				$XML_ID = isset($tmp['Ид']) ? $tmp['Ид'] : '';
				if ($XML_ID == '') continue;

				// prepare $arFields and $arProperties
				$arFields = array();
				$arProperties = array();
				foreach ($map['FIELDS'] as $mapField) {
					if (isset($tmp[$mapField['XML_NODE']])) {
						if (isset($mapField['BX_FIELD'])) {
							$arFields[$mapField['BX_FIELD']] = $tmp[$mapField['XML_NODE']];
						}
						if (isset($mapField['BX_PROPERTY'])) {
							$p = $mapField['BX_PROPERTY'];
							$v = $tmp[$mapField['XML_NODE']];
							if (!isset($IBLOCK_PROPS[$p]) && $map['CREATE_BX_PROPERTIES'] == 'Y') {
								$propFields = array(
									'NAME' => $mapField['XML_NODE'],
									'CODE' => $p,
									'ACTIVE' => 'Y',
									'SORT' => '500',
									'PROPERTY_TYPE' => 'S',
									'IBLOCK_ID' => $IBLOCK_ID,
								);
								$ibp = new CIBlockProperty;
								if ($ibp->Add($propFields)) {
									$IBLOCK_PROPS[$p] = $propFields;
								}
							}
							switch ($IBLOCK_PROPS[$p]['PROPERTY_TYPE']) {
								case 'E':
									$LINK_IBLOCK_ID = $IBLOCK_PROPS[$p]['LINK_IBLOCK_ID'];
									if ($XML_ID != $v) {
										$filter = array('XML_ID' => $v, 'IBLOCK_ID' => $LINK_IBLOCK_ID);
										if ($element = CIBlockElement::GetList(array(), $filter, false, false, array('ID'))->Fetch()) {
											$v = $element['ID'];
										} else {
											$v = '';
										}
									} else {
										$v = '';
									}
									break;
								case 'N':
									$v['VALUE'] = floatval($v);
									break;
								case 'S':
									if (isset($IBLOCK_PROPS[$p]['USER_TYPE']) && $IBLOCK_PROPS[$p]['USER_TYPE'] == 'DateTime') {
										$v = date('d.m.Y H:i', strtotime($v));
									} elseif ($v == 'true') {
										$v = 'Y';
									} elseif ($v == 'false') {
										$v = 'N';
									}
									break;
								default:
									//
									break;
							}
							$arProperties[$p] = $v;
						}
					}
				}

				if ($arFields['NAME'] == '') continue;
				$arFields['ACTIVE'] = 'Y';

				// update or add element
				$el = new \CIBlockElement;
				$dbItems = \CIBlockElement::GetList([], ['XML_ID' => $XML_ID, 'IBLOCK_ID' => $IBLOCK_ID], false, false, ['ID', 'XML_ID']);
				if ($item = $dbItems->GetNext()) {
					$ELEMENT_ID = $item['ID'];
					if (!$el->Update($ELEMENT_ID, $arFields)) {
						$this->abortImport($el->LAST_ERROR);
					}
					foreach ($arProperties as $code => $value) {
						\CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($code => $value));
					}
				} else {
					$arFields['IBLOCK_ID'] = $IBLOCK_ID;
					$arFields['XML_ID'] = $XML_ID;
					$ELEMENT_ID = $el->Add($arFields);
					if ($ELEMENT_ID <= 0) {
						$this->abortImport($el->LAST_ERROR);
					}
					foreach ($arProperties as $code => $value) {
						\CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($code => $value));
					}
				}
			}
		} 
		$this->logEvent('Импорт справочника ' . $map['XML_ROOT'] . ' завершен.');
	}

 	public function processFile() {

 		ini_set('max_execution_time', 600);
 		set_time_limit(600);

		\Bitrix\Main\Loader::includeModule('iblock');
		$this->obXMLFile = new \CIBlockXMLFile;

		$this->obXMLFile->DropTemporaryTables();
		$this->logEvent('Временные таблицы удалены.');

		$this->obXMLFile->CreateTemporaryTables();
		$this->logEvent('Временные таблицы созданы.');

		$this->importToIBlock($this->IMPORT_MAP['THERMAL_RANGE']);

		$this->importToIBlock($this->IMPORT_MAP['CONTRAGENT']);
		$this->importToIBlock($this->IMPORT_MAP['ADDRESS']);
		$this->importToIBlock($this->IMPORT_MAP['CONTACT']);
		$this->importToIBlock($this->IMPORT_MAP['THERMAL_RANGE']);
		$this->importToIBlock($this->IMPORT_MAP['PACKAGE']);
		$this->importToIBlock($this->IMPORT_MAP['COMPATIBILITY_GROUP']);
		$this->importToIBlock($this->IMPORT_MAP['NOMENCLATURE']);
		$this->importToIBlock($this->IMPORT_MAP['APPLICATION']);

		return $this->finishImport();
 	}
}
