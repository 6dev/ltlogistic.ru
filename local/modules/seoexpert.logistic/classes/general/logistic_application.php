<?php
namespace Logistic;
use \Bitrix\Main\Config\Option;

class Application {
	
	const MODULE_ID = 'seoexpert.logistic';

	static function generateNumber() {
		$numberPrefix = trim(ToUpper(Option::get(self::MODULE_ID, 'application_number_prefix')));
		$nextNumber = intval(Option::get(self::MODULE_ID, 'application_next_number'));
		Option::set(self::MODULE_ID, 'application_next_number', $nextNumber + 1);
		$digitsCount = intval(Option::get(self::MODULE_ID, 'application_number_digits_count'));
		if ($digitsCount <= 0) {
			$digitsCount = 1;
		}
 		$result = sprintf('%0' . $digitsCount . 'd', $nextNumber);
 		if ($numberPrefix != '') {
 			  $result = $numberPrefix . '-' . $result;
 		}
		return $result;
	}

	static function getProperties() {
		$result = array();


		return $result;
	}

	static function getPermissions($id) {
		if ($id <= 0) return false;

		$result = array(
		'VIEW' => 'Y',
		'EDIT' => 'Y',
		'CANCEL' => 'Y',
		);

		\Bitrix\Main\Loader::includeModule('iblock');
		$dbApplications = \CIBlockElement::GetList(array(), array('ACTIVE' => 'Y', 'ID' => $id, 'IBLOCK_ID' => APPLICATION_IBLOCK_ID), false, false);
		if ($application = $dbApplications->GetNextElement()) {
		$applicationProperties = $application->GetProperties();
		if ($applicationProperties['LOCKED']['VALUE'] == 'Y' || $applicationProperties['CANCELLED']['VALUE'] == 'Y') {
		$result['EDIT'] = 'N';
		$result['CANCEL'] = 'N';
		}
		$d = strtotime(date('d.m.Y 17:00:00', strtotime('-1 day', strtotime($applicationProperties['SHIPMENT_DATE']['VALUE']))));
		if (time() >= $d) {
		$result['EDIT'] = 'N';
		$result['CANCEL'] = 'N';
		}

		// check for group A
		}

		return $result;
		}

		static function fetchById($id) {
			\Bitrix\Main\Loader::includeModule('iblock');
			$dbItems = \CIBlockElement::GetList(array('ID' => 'DESC'), array('ID' => $id, 'IBLOCK_ID' => APPLICATION_IBLOCK_ID), false, false);
			if ($obItem = $dbItems->GetNextElement()) {
				$item = array();
			    $fields = $obItem->GetFields();
			    $item['ID'] = $fields['ID'];
				$props = $obItem->GetProperties();
			    foreach ($props as $param => $v) {
			        $value = $v['VALUE'];
			        // link to iblock element
			        if ($v['PROPERTY_TYPE'] == 'E' && $v['VALUE'] > 0) {
						$dbLinkedItems = \CIBlockElement::GetList(array('ID' => 'DESC'), array('=ID' => $value ,'IBLOCK_ID' => $v['LINK_IBLOCK_ID']), false, false);
						if ($linkedItem = $dbLinkedItems->GetNextElement()) {
							$linkedItemFields = $linkedItem->GetFields();
							$linkedItemProperties = $linkedItem->GetProperties();
							$item[$param . '.ID'] = $linkedItemFields['ID'];
							foreach ($linkedItemProperties as $linkedItemProperty) {
								$item[$param . '.' . $linkedItemProperty['CODE']] = $linkedItemProperty['VALUE'];
							}
							$value = $linkedItemFields['NAME'];
						} else {
			                $value = '';
			                $item[$param . '.ID'] = 0;
			            }
			            /*$dbLinkedItem = \CIBlockElement::GetById($value);
			            if ($linkedItem = $dbLinkedItem->Fetch()) {
			            	//var_dump($linkedItem);
			            	//die();
			                $value = $linkedItem['NAME'];
			            } else {
			                $value = '';
			            }*/
			        }
			        // link to user
			        if ($v['PROPERTY_TYPE'] == 'S' && $v['USER_TYPE'] == 'UserID' && $v['VALUE'] > 0) {
			            $dbUser = \CUser::GetById($value);
			            if ($user = $dbUser->Fetch()) {
			                $value = $user['NAME'];
			            } else {
			                $value = '';
			            }
			        }
			        $item[$param] = $value;
			    }
			    // tmp
    			$item['NUMBER'] = $fields['NAME'];
			    return $item;
			} else {
				return false;
			}
		}



		static function fetchLists($ELEMENT_ID) {

			\Bitrix\Main\Loader::includeModule('iblock');
			$lists = array();
			foreach (array('PALLET_LOAD', 'FREIGHT_TRAIN', 'SERVICES') as $listName) {
				switch ($listName) {
					case 'PALLET_LOAD': $IBLOCK_ID = 28; break;
					case 'FREIGHT_TRAIN': $IBLOCK_ID = 29; break;
					case 'SERVICES': $IBLOCK_ID = 30; break;
				}
				$props = array();
				$elements = array();
				$dbProperties = \CIBlockProperty::GetList(array("SORT" => "ASC", "ID" => "ASC"), array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID));
				while ($property = $dbProperties->GetNext()) {
					if ($property['CODE'] == 'LINK') continue;
					if ($property['LIST_TYPE'] == 'L' && $property['LINK_IBLOCK_ID']) {
						$tmp = array();
						$filter = array('IBLOCK_ID' => $property['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y');
						if ($property['CODE'] == 'SERVICE_TYPE') {
							$filter['PROPERTY_USLUGA'] = 'Y';
						} elseif ($property['CODE'] == 'TS_NOMENCLATURE') {
							$filter['!PROPERTY_USLUGA'] = 'Y';
						}
						$dbLinkedItems = \CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $filter, false, false, array('ID', 'NAME'));
						while ($arLinkedItem = $dbLinkedItems->GetNext()) {
							$tmp[$arLinkedItem['ID']] = $arLinkedItem['NAME'];
						}
						$property['VALUES'] = $tmp;
					}
					$props[] = $property;
				//echo $prop_fields["ID"]." - ".$prop_fields["NAME"]."<br>";
				}

				$elements = array();
				$dbElements = \CIBlockElement::GetList(array('ID' => 'ASC'), array('IBLOCK_ID' => $IBLOCK_ID, 'PROPERTY_LINK' => $ELEMENT_ID), false, false);
				while ($dbElement = $dbElements->GetNextElement()) {
					$element = array();
					$fields = $dbElement->GetFields();
					$properties = $dbElement->GetProperties();
					foreach ($properties as $property) {
						$element[$property['CODE']] = $property['VALUE'];
					}
					$element['ID'] = $fields['ID'];
					$elements[] = $element;
				}

				$lists[$listName] = array(
					'IBLOCK_ID' => $IBLOCK_ID,
					'PROPERTIES' => $props,
					'ELEMENTS' => $elements,
				);
			}
			return $lists;
		}
		
}
