<?php
namespace Logistic;
use \Bitrix\Main\Config\Option;

class Contragent {
	
	const MODULE_ID = 'seoexpert.logistic';

	public static function getList($arOrder, $arFilter, $w, $arNavParams, $arSelect) {
		if (!is_array($arFilter)) {
			$arFilter = array();
		}
		$arFilter['IBLOCK_ID'] = CONTRAGENT_IBLOCK_ID;
		\Bitrix\Main\Loader::includeModule('iblock');
		return \CIBlockElement::GetList($arOrder, $arFilter, false, $arNavParams, $arSelect);
	}

	static function fetchById($id) {
		\Bitrix\Main\Loader::includeModule('iblock');
		$dbItems = \CIBlockElement::GetList(array('ID' => 'DESC'), array('ID' => $id, 'IBLOCK_ID' => CONTRAGENT_IBLOCK_ID), false, false);
		if ($obItem = $dbItems->GetNextElement()) {
			$item = array();
		    $fields = $obItem->GetFields();
		    $item['ID'] = $fields['ID'];
			$props = $obItem->GetProperties();
		    foreach ($props as $param => $v) {
		        $value = $v['VALUE'];
		        // link to iblock element
		        if ($v['PROPERTY_TYPE'] == 'E') {
		            $dbLinkedItem = \CIBlockElement::GetById($value);
		            if ($linkedItem = $dbLinkedItem->Fetch()) {
		                $value = $linkedItem['NAME'];
		            } else {
		                $value = '';
		            }
		        }
		        // link to user
		        if ($v['PROPERTY_TYPE'] == 'S' && $v['USER_TYPE'] == 'UserID') {
		            $dbUser = \CUser::GetById($value);
		            if ($user = $dbUser->Fetch()) {
		                $value = $user['NAME'];
		            } else {
		                $value = '';
		            }
		        }
		        $item[$param] = $value;
		    }
		    return $item;
		} else {
			return false;
		}
	}

}
