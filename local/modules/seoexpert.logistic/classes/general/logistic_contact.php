<?php
namespace Logistic;
use \Bitrix\Main\Config\Option;

class Contact { 
	
	const MODULE_ID = 'seoexpert.logistic';

	public static function getList($arOrder, $arFilter, $w, $arNavParams, $arSelect) {
		if (!is_array($arFilter)) {
			$arFilter = array();
		}
		$arFilter['IBLOCK_ID'] = CONTACT_IBLOCK_ID;
		\Bitrix\Main\Loader::includeModule('iblock');
		return \CIBlockElement::GetList($arOrder, $arFilter, false, $arNavParams, $arSelect);
	}

}