<?php
static $MODULE_ID = 'seoexpert.logistic';
\Bitrix\Main\Loader::includeModule($MODULE_ID);
\Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\Logistic\Application' => "/local/modules/{$MODULE_ID}/classes/general/logistic_application.php",
    '\Logistic\Address' => "/local/modules/{$MODULE_ID}/classes/general/logistic_address.php",
    '\Logistic\Contragent' => "/local/modules/{$MODULE_ID}/classes/general/logistic_contragent.php",
    '\Logistic\Contact' => "/local/modules/{$MODULE_ID}/classes/general/logistic_contact.php",
    '\Logistic\Exchange1C' => "/local/modules/{$MODULE_ID}/classes/general/logistic_exchange_1c.php",
));
